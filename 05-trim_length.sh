OUT=05-trim_length
DATA_DIR=data
SPLIT_DIR=03-split_by_reference
ADAPTER_DIR=01-trimmed_adapters
QUALITY_DIR=04-trim_quality

three_read1=AGATCGGAAGAGCACACGTCTGAACTCCAGTCACNNNNNNATCTCGTATGCCGTCTTCTGCTTG
three_read2=AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTAGATCTCGGTGGTCGCCGTATCATT
five_read1=AATGATACGGCGACCACCGAGATCTACACTCTTTCCCTACACGACGCTCTTCCGATCT
five_read2=GTTCGTCTTCTGCCGTATGCTCTANNNNNNCACTGACCTCAAGTCTGCACACGAGAAGGCTAG

function call_ca {
	cutadapt \
			--quality-cutoff=25 \
			--minimum-length 100 \
			--length 100 \
			-o "${OUT}/$3" \
			-p "${OUT}/$4" \
			"${QUALITY_DIR}/$1" "${QUALITY_DIR}/$2" \
			> "${OUT}/$5"
	exit
}








call_ca inplanta-3KO-020-R1-atrimmed-unmapped-qtrimmed.fastq.gz inplanta-3KO-020-R2-atrimmed-unmapped-qtrimmed.fastq.gz inplanta-3KO-020-R1-atrimmed-unmapped-qtrimmed-fixed.fastq.gz inplanta-3KO-020-R2-atrimmed-unmapped-qtrimmed-fixed.fastq.gz inplanta-3KO-020-atrimmed-unmapped-qtrimmed-fixed.log &
call_ca inplanta-3KO-021-R1-atrimmed-unmapped-qtrimmed.fastq.gz inplanta-3KO-021-R2-atrimmed-unmapped-qtrimmed.fastq.gz inplanta-3KO-021-R1-atrimmed-unmapped-qtrimmed-fixed.fastq.gz inplanta-3KO-021-R2-atrimmed-unmapped-qtrimmed-fixed.fastq.gz inplanta-3KO-021-atrimmed-unmapped-qtrimmed-fixed.log &
call_ca inplanta-3KO-022-R1-atrimmed-unmapped-qtrimmed.fastq.gz inplanta-3KO-022-R2-atrimmed-unmapped-qtrimmed.fastq.gz inplanta-3KO-022-R1-atrimmed-unmapped-qtrimmed-fixed.fastq.gz inplanta-3KO-022-R2-atrimmed-unmapped-qtrimmed-fixed.fastq.gz inplanta-3KO-022-atrimmed-unmapped-qtrimmed-fixed.log &
call_ca inplanta-3KO-023-R1-atrimmed-unmapped-qtrimmed.fastq.gz inplanta-3KO-023-R2-atrimmed-unmapped-qtrimmed.fastq.gz inplanta-3KO-023-R1-atrimmed-unmapped-qtrimmed-fixed.fastq.gz inplanta-3KO-023-R2-atrimmed-unmapped-qtrimmed-fixed.fastq.gz inplanta-3KO-023-atrimmed-unmapped-qtrimmed-fixed.log &
call_ca inplanta-Pf2-024-R1-atrimmed-unmapped-qtrimmed.fastq.gz inplanta-Pf2-024-R2-atrimmed-unmapped-qtrimmed.fastq.gz inplanta-Pf2-024-R1-atrimmed-unmapped-qtrimmed-fixed.fastq.gz inplanta-Pf2-024-R2-atrimmed-unmapped-qtrimmed-fixed.fastq.gz inplanta-Pf2-024-atrimmed-unmapped-qtrimmed-fixed.log &
call_ca inplanta-Pf2-026-R1-atrimmed-unmapped-qtrimmed.fastq.gz inplanta-Pf2-026-R2-atrimmed-unmapped-qtrimmed.fastq.gz inplanta-Pf2-026-R1-atrimmed-unmapped-qtrimmed-fixed.fastq.gz inplanta-Pf2-026-R2-atrimmed-unmapped-qtrimmed-fixed.fastq.gz inplanta-Pf2-026-atrimmed-unmapped-qtrimmed-fixed.log &
call_ca inplanta-Pf2-025-R1-atrimmed-unmapped-qtrimmed.fastq.gz inplanta-Pf2-025-R2-atrimmed-unmapped-qtrimmed.fastq.gz inplanta-Pf2-025-R1-atrimmed-unmapped-qtrimmed-fixed.fastq.gz inplanta-Pf2-025-R2-atrimmed-unmapped-qtrimmed-fixed.fastq.gz inplanta-Pf2-025-atrimmed-unmapped-qtrimmed-fixed.log &
call_ca inplanta-Pf2-027-R1-atrimmed-unmapped-qtrimmed.fastq.gz inplanta-Pf2-027-R2-atrimmed-unmapped-qtrimmed.fastq.gz inplanta-Pf2-027-R1-atrimmed-unmapped-qtrimmed-fixed.fastq.gz inplanta-Pf2-027-R2-atrimmed-unmapped-qtrimmed-fixed.fastq.gz inplanta-Pf2-027-atrimmed-unmapped-qtrimmed-fixed.log &
call_ca inplanta-SN15-016-R1-atrimmed-unmapped-qtrimmed.fastq.gz inplanta-SN15-016-R2-atrimmed-unmapped-qtrimmed.fastq.gz inplanta-SN15-016-R1-atrimmed-unmapped-qtrimmed-fixed.fastq.gz inplanta-SN15-016-R2-atrimmed-unmapped-qtrimmed-fixed.fastq.gz inplanta-SN15-016-atrimmed-unmapped-qtrimmed-fixed.log &
call_ca inplanta-SN15-017-R1-atrimmed-unmapped-qtrimmed.fastq.gz inplanta-SN15-017-R2-atrimmed-unmapped-qtrimmed.fastq.gz inplanta-SN15-017-R1-atrimmed-unmapped-qtrimmed-fixed.fastq.gz inplanta-SN15-017-R2-atrimmed-unmapped-qtrimmed-fixed.fastq.gz inplanta-SN15-017-atrimmed-unmapped-qtrimmed-fixed.log &
call_ca inplanta-SN15-018-R1-atrimmed-unmapped-qtrimmed.fastq.gz inplanta-SN15-018-R2-atrimmed-unmapped-qtrimmed.fastq.gz inplanta-SN15-018-R1-atrimmed-unmapped-qtrimmed-fixed.fastq.gz inplanta-SN15-018-R2-atrimmed-unmapped-qtrimmed-fixed.fastq.gz inplanta-SN15-018-atrimmed-unmapped-qtrimmed-fixed.log &
call_ca inplanta-SN15-019-R1-atrimmed-unmapped-qtrimmed.fastq.gz inplanta-SN15-019-R2-atrimmed-unmapped-qtrimmed.fastq.gz inplanta-SN15-019-R1-atrimmed-unmapped-qtrimmed-fixed.fastq.gz inplanta-SN15-019-R2-atrimmed-unmapped-qtrimmed-fixed.fastq.gz inplanta-SN15-019-atrimmed-unmapped-qtrimmed-fixed.log &

wait

call_ca inplanta-3KO-020-R1-atrimmed-ta-qtrimmed.fastq.gz inplanta-3KO-020-R2-atrimmed-ta-qtrimmed.fastq.gz inplanta-3KO-020-R1-atrimmed-ta-qtrimmed-fixed.fastq.gz inplanta-3KO-020-R2-atrimmed-ta-qtrimmed-fixed.fastq.gz inplanta-3KO-020-atrimmed-ta-qtrimmed-fixed.log &
call_ca inplanta-3KO-021-R1-atrimmed-ta-qtrimmed.fastq.gz inplanta-3KO-021-R2-atrimmed-ta-qtrimmed.fastq.gz inplanta-3KO-021-R1-atrimmed-ta-qtrimmed-fixed.fastq.gz inplanta-3KO-021-R2-atrimmed-ta-qtrimmed-fixed.fastq.gz inplanta-3KO-021-atrimmed-ta-qtrimmed-fixed.log &
call_ca inplanta-3KO-022-R1-atrimmed-ta-qtrimmed.fastq.gz inplanta-3KO-022-R2-atrimmed-ta-qtrimmed.fastq.gz inplanta-3KO-022-R1-atrimmed-ta-qtrimmed-fixed.fastq.gz inplanta-3KO-022-R2-atrimmed-ta-qtrimmed-fixed.fastq.gz inplanta-3KO-022-atrimmed-ta-qtrimmed-fixed.log &
call_ca inplanta-3KO-023-R1-atrimmed-ta-qtrimmed.fastq.gz inplanta-3KO-023-R2-atrimmed-ta-qtrimmed.fastq.gz inplanta-3KO-023-R1-atrimmed-ta-qtrimmed-fixed.fastq.gz inplanta-3KO-023-R2-atrimmed-ta-qtrimmed-fixed.fastq.gz inplanta-3KO-023-atrimmed-ta-qtrimmed-fixed.log &
call_ca inplanta-Pf2-024-R1-atrimmed-ta-qtrimmed.fastq.gz inplanta-Pf2-024-R2-atrimmed-ta-qtrimmed.fastq.gz inplanta-Pf2-024-R1-atrimmed-ta-qtrimmed-fixed.fastq.gz inplanta-Pf2-024-R2-atrimmed-ta-qtrimmed-fixed.fastq.gz inplanta-Pf2-024-atrimmed-ta-qtrimmed-fixed.log &
call_ca inplanta-Pf2-025-R1-atrimmed-ta-qtrimmed.fastq.gz inplanta-Pf2-025-R2-atrimmed-ta-qtrimmed.fastq.gz inplanta-Pf2-025-R1-atrimmed-ta-qtrimmed-fixed.fastq.gz inplanta-Pf2-025-R2-atrimmed-ta-qtrimmed-fixed.fastq.gz inplanta-Pf2-025-atrimmed-ta-qtrimmed-fixed.log &
call_ca inplanta-Pf2-026-R1-atrimmed-ta-qtrimmed.fastq.gz inplanta-Pf2-026-R2-atrimmed-ta-qtrimmed.fastq.gz inplanta-Pf2-026-R1-atrimmed-ta-qtrimmed-fixed.fastq.gz inplanta-Pf2-026-R2-atrimmed-ta-qtrimmed-fixed.fastq.gz inplanta-Pf2-026-atrimmed-ta-qtrimmed-fixed.log &
call_ca inplanta-Pf2-027-R1-atrimmed-ta-qtrimmed.fastq.gz inplanta-Pf2-027-R2-atrimmed-ta-qtrimmed.fastq.gz inplanta-Pf2-027-R1-atrimmed-ta-qtrimmed-fixed.fastq.gz inplanta-Pf2-027-R2-atrimmed-ta-qtrimmed-fixed.fastq.gz inplanta-Pf2-027-atrimmed-ta-qtrimmed-fixed.log &
call_ca inplanta-SN15-016-R1-atrimmed-ta-qtrimmed.fastq.gz inplanta-SN15-016-R2-atrimmed-ta-qtrimmed.fastq.gz inplanta-SN15-016-R1-atrimmed-ta-qtrimmed-fixed.fastq.gz inplanta-SN15-016-R2-atrimmed-ta-qtrimmed-fixed.fastq.gz inplanta-SN15-016-atrimmed-ta-qtrimmed-fixed.log &
call_ca inplanta-SN15-017-R1-atrimmed-ta-qtrimmed.fastq.gz inplanta-SN15-017-R2-atrimmed-ta-qtrimmed.fastq.gz inplanta-SN15-017-R1-atrimmed-ta-qtrimmed-fixed.fastq.gz inplanta-SN15-017-R2-atrimmed-ta-qtrimmed-fixed.fastq.gz inplanta-SN15-017-atrimmed-ta-qtrimmed-fixed.log &
call_ca inplanta-SN15-018-R1-atrimmed-ta-qtrimmed.fastq.gz inplanta-SN15-018-R2-atrimmed-ta-qtrimmed.fastq.gz inplanta-SN15-018-R1-atrimmed-ta-qtrimmed-fixed.fastq.gz inplanta-SN15-018-R2-atrimmed-ta-qtrimmed-fixed.fastq.gz inplanta-SN15-018-atrimmed-ta-qtrimmed-fixed.log &
call_ca inplanta-SN15-019-R1-atrimmed-ta-qtrimmed.fastq.gz inplanta-SN15-019-R2-atrimmed-ta-qtrimmed.fastq.gz inplanta-SN15-019-R1-atrimmed-ta-qtrimmed-fixed.fastq.gz inplanta-SN15-019-R2-atrimmed-ta-qtrimmed-fixed.fastq.gz inplanta-SN15-019-atrimmed-ta-qtrimmed-fixed.log &
call_ca inplanta-Tween-012-R1-atrimmed-ta-qtrimmed.fastq.gz inplanta-Tween-012-R2-atrimmed-ta-qtrimmed.fastq.gz inplanta-Tween-012-R1-atrimmed-ta-qtrimmed-fixed.fastq.gz inplanta-Tween-012-R2-atrimmed-ta-qtrimmed-fixed.fastq.gz inplanta-Tween-012-atrimmed-ta-qtrimmed-fixed.log &
call_ca inplanta-Tween-013-R1-atrimmed-ta-qtrimmed.fastq.gz inplanta-Tween-013-R2-atrimmed-ta-qtrimmed.fastq.gz inplanta-Tween-013-R1-atrimmed-ta-qtrimmed-fixed.fastq.gz inplanta-Tween-013-R2-atrimmed-ta-qtrimmed-fixed.fastq.gz inplanta-Tween-013-atrimmed-ta-qtrimmed-fixed.log &
call_ca inplanta-Tween-014-R1-atrimmed-ta-qtrimmed.fastq.gz inplanta-Tween-014-R2-atrimmed-ta-qtrimmed.fastq.gz inplanta-Tween-014-R1-atrimmed-ta-qtrimmed-fixed.fastq.gz inplanta-Tween-014-R2-atrimmed-ta-qtrimmed-fixed.fastq.gz inplanta-Tween-014-atrimmed-ta-qtrimmed-fixed.log &
call_ca inplanta-Tween-015-R1-atrimmed-ta-qtrimmed.fastq.gz inplanta-Tween-015-R2-atrimmed-ta-qtrimmed.fastq.gz inplanta-Tween-015-R1-atrimmed-ta-qtrimmed-fixed.fastq.gz inplanta-Tween-015-R2-atrimmed-ta-qtrimmed-fixed.fastq.gz inplanta-Tween-015-atrimmed-ta-qtrimmed-fixed.log &

wait

call_ca inplanta-3KO-020-R1-atrimmed-pn-qtrimmed.fastq.gz inplanta-3KO-020-R2-atrimmed-pn-qtrimmed.fastq.gz inplanta-3KO-020-R1-atrimmed-pn-qtrimmed-fixed.fastq.gz inplanta-3KO-020-R2-atrimmed-pn-qtrimmed-fixed.fastq.gz inplanta-3KO-020-atrimmed-pn-qtrimmed-fixed.log &
call_ca inplanta-3KO-021-R1-atrimmed-pn-qtrimmed.fastq.gz inplanta-3KO-021-R2-atrimmed-pn-qtrimmed.fastq.gz inplanta-3KO-021-R1-atrimmed-pn-qtrimmed-fixed.fastq.gz inplanta-3KO-021-R2-atrimmed-pn-qtrimmed-fixed.fastq.gz inplanta-3KO-021-atrimmed-pn-qtrimmed-fixed.log &
call_ca inplanta-3KO-022-R1-atrimmed-pn-qtrimmed.fastq.gz inplanta-3KO-022-R2-atrimmed-pn-qtrimmed.fastq.gz inplanta-3KO-022-R1-atrimmed-pn-qtrimmed-fixed.fastq.gz inplanta-3KO-022-R2-atrimmed-pn-qtrimmed-fixed.fastq.gz inplanta-3KO-022-atrimmed-pn-qtrimmed-fixed.log &
call_ca inplanta-3KO-023-R1-atrimmed-pn-qtrimmed.fastq.gz inplanta-3KO-023-R2-atrimmed-pn-qtrimmed.fastq.gz inplanta-3KO-023-R1-atrimmed-pn-qtrimmed-fixed.fastq.gz inplanta-3KO-023-R2-atrimmed-pn-qtrimmed-fixed.fastq.gz inplanta-3KO-023-atrimmed-pn-qtrimmed-fixed.log &
call_ca inplanta-Pf2-024-R1-atrimmed-pn-qtrimmed.fastq.gz inplanta-Pf2-024-R2-atrimmed-pn-qtrimmed.fastq.gz inplanta-Pf2-024-R1-atrimmed-pn-qtrimmed-fixed.fastq.gz inplanta-Pf2-024-R2-atrimmed-pn-qtrimmed-fixed.fastq.gz inplanta-Pf2-024-atrimmed-pn-qtrimmed-fixed.log &
call_ca inplanta-Pf2-025-R1-atrimmed-pn-qtrimmed.fastq.gz inplanta-Pf2-025-R2-atrimmed-pn-qtrimmed.fastq.gz inplanta-Pf2-025-R1-atrimmed-pn-qtrimmed-fixed.fastq.gz inplanta-Pf2-025-R2-atrimmed-pn-qtrimmed-fixed.fastq.gz inplanta-Pf2-025-atrimmed-pn-qtrimmed-fixed.log &
call_ca inplanta-Pf2-026-R1-atrimmed-pn-qtrimmed.fastq.gz inplanta-Pf2-026-R2-atrimmed-pn-qtrimmed.fastq.gz inplanta-Pf2-026-R1-atrimmed-pn-qtrimmed-fixed.fastq.gz inplanta-Pf2-026-R2-atrimmed-pn-qtrimmed-fixed.fastq.gz inplanta-Pf2-026-atrimmed-pn-qtrimmed-fixed.log &
call_ca inplanta-Pf2-027-R1-atrimmed-pn-qtrimmed.fastq.gz inplanta-Pf2-027-R2-atrimmed-pn-qtrimmed.fastq.gz inplanta-Pf2-027-R1-atrimmed-pn-qtrimmed-fixed.fastq.gz inplanta-Pf2-027-R2-atrimmed-pn-qtrimmed-fixed.fastq.gz inplanta-Pf2-027-atrimmed-pn-qtrimmed-fixed.log &
call_ca inplanta-SN15-016-R1-atrimmed-pn-qtrimmed.fastq.gz inplanta-SN15-016-R2-atrimmed-pn-qtrimmed.fastq.gz inplanta-SN15-016-R1-atrimmed-pn-qtrimmed-fixed.fastq.gz inplanta-SN15-016-R2-atrimmed-pn-qtrimmed-fixed.fastq.gz inplanta-SN15-016-atrimmed-pn-qtrimmed-fixed.log &
call_ca inplanta-SN15-017-R1-atrimmed-pn-qtrimmed.fastq.gz inplanta-SN15-017-R2-atrimmed-pn-qtrimmed.fastq.gz inplanta-SN15-017-R1-atrimmed-pn-qtrimmed-fixed.fastq.gz inplanta-SN15-017-R2-atrimmed-pn-qtrimmed-fixed.fastq.gz inplanta-SN15-017-atrimmed-pn-qtrimmed-fixed.log &
call_ca inplanta-SN15-018-R1-atrimmed-pn-qtrimmed.fastq.gz inplanta-SN15-018-R2-atrimmed-pn-qtrimmed.fastq.gz inplanta-SN15-018-R1-atrimmed-pn-qtrimmed-fixed.fastq.gz inplanta-SN15-018-R2-atrimmed-pn-qtrimmed-fixed.fastq.gz inplanta-SN15-018-atrimmed-pn-qtrimmed-fixed.log &
call_ca inplanta-SN15-019-R1-atrimmed-pn-qtrimmed.fastq.gz inplanta-SN15-019-R2-atrimmed-pn-qtrimmed.fastq.gz inplanta-SN15-019-R1-atrimmed-pn-qtrimmed-fixed.fastq.gz inplanta-SN15-019-R2-atrimmed-pn-qtrimmed-fixed.fastq.gz inplanta-SN15-019-atrimmed-pn-qtrimmed-fixed.log &
wait

call_ca invitro-3KO-004-R1-atrimmed-pn-qtrimmed.fastq.gz invitro-3KO-004-R2-atrimmed-pn-qtrimmed.fastq.gz invitro-3KO-004-R1-atrimmed-pn-qtrimmed-fixed.fastq.gz invitro-3KO-004-R2-atrimmed-pn-qtrimmed-fixed.fastq.gz invitro-3KO-004-atrimmed-pn-qtrimmed.log &
call_ca invitro-3KO-005-R1-atrimmed-pn-qtrimmed.fastq.gz invitro-3KO-005-R2-atrimmed-pn-qtrimmed.fastq.gz invitro-3KO-005-R1-atrimmed-pn-qtrimmed-fixed.fastq.gz invitro-3KO-005-R2-atrimmed-pn-qtrimmed-fixed.fastq.gz invitro-3KO-005-atrimmed-pn-qtrimmed.log &
call_ca invitro-3KO-006-R1-atrimmed-pn-qtrimmed.fastq.gz invitro-3KO-006-R2-atrimmed-pn-qtrimmed.fastq.gz invitro-3KO-006-R1-atrimmed-pn-qtrimmed-fixed.fastq.gz invitro-3KO-006-R2-atrimmed-pn-qtrimmed-fixed.fastq.gz invitro-3KO-006-atrimmed-pn-qtrimmed.log &
call_ca invitro-3KO-007-R1-atrimmed-pn-qtrimmed.fastq.gz invitro-3KO-007-R2-atrimmed-pn-qtrimmed.fastq.gz invitro-3KO-007-R1-atrimmed-pn-qtrimmed-fixed.fastq.gz invitro-3KO-007-R2-atrimmed-pn-qtrimmed-fixed.fastq.gz invitro-3KO-007-atrimmed-pn-qtrimmed.log &
call_ca invitro-Pf2-008-R1-atrimmed-pn-qtrimmed.fastq.gz invitro-Pf2-008-R2-atrimmed-pn-qtrimmed.fastq.gz invitro-Pf2-008-R1-atrimmed-pn-qtrimmed-fixed.fastq.gz invitro-Pf2-008-R2-atrimmed-pn-qtrimmed-fixed.fastq.gz invitro-Pf2-008-atrimmed-pn-qtrimmed.log &
call_ca invitro-Pf2-009-R1-atrimmed-pn-qtrimmed.fastq.gz invitro-Pf2-009-R2-atrimmed-pn-qtrimmed.fastq.gz invitro-Pf2-009-R1-atrimmed-pn-qtrimmed-fixed.fastq.gz invitro-Pf2-009-R2-atrimmed-pn-qtrimmed-fixed.fastq.gz invitro-Pf2-009-atrimmed-pn-qtrimmed.log &
call_ca invitro-Pf2-010-R1-atrimmed-pn-qtrimmed.fastq.gz invitro-Pf2-010-R2-atrimmed-pn-qtrimmed.fastq.gz invitro-Pf2-010-R1-atrimmed-pn-qtrimmed-fixed.fastq.gz invitro-Pf2-010-R2-atrimmed-pn-qtrimmed-fixed.fastq.gz invitro-Pf2-010-atrimmed-pn-qtrimmed.log &
call_ca invitro-Pf2-011-R1-atrimmed-pn-qtrimmed.fastq.gz invitro-Pf2-011-R2-atrimmed-pn-qtrimmed.fastq.gz invitro-Pf2-011-R1-atrimmed-pn-qtrimmed-fixed.fastq.gz invitro-Pf2-011-R2-atrimmed-pn-qtrimmed-fixed.fastq.gz invitro-Pf2-011-atrimmed-pn-qtrimmed.log &
call_ca invitro-SN15-000-R1-atrimmed-pn-qtrimmed.fastq.gz invitro-SN15-000-R2-atrimmed-pn-qtrimmed.fastq.gz invitro-SN15-000-R1-atrimmed-pn-qtrimmed-fixed.fastq.gz invitro-SN15-000-R2-atrimmed-pn-qtrimmed-fixed.fastq.gz invitro-SN15-000-atrimmed-pn-qtrimmed.log &
call_ca invitro-SN15-001-R1-atrimmed-pn-qtrimmed.fastq.gz invitro-SN15-001-R2-atrimmed-pn-qtrimmed.fastq.gz invitro-SN15-001-R1-atrimmed-pn-qtrimmed-fixed.fastq.gz invitro-SN15-001-R2-atrimmed-pn-qtrimmed-fixed.fastq.gz invitro-SN15-001-atrimmed-pn-qtrimmed.log &
call_ca invitro-SN15-002-R1-atrimmed-pn-qtrimmed.fastq.gz invitro-SN15-002-R2-atrimmed-pn-qtrimmed.fastq.gz invitro-SN15-002-R1-atrimmed-pn-qtrimmed-fixed.fastq.gz invitro-SN15-002-R2-atrimmed-pn-qtrimmed-fixed.fastq.gz invitro-SN15-002-atrimmed-pn-qtrimmed.log &
call_ca invitro-SN15-003-R1-atrimmed-pn-qtrimmed.fastq.gz invitro-SN15-003-R2-atrimmed-pn-qtrimmed.fastq.gz invitro-SN15-003-R1-atrimmed-pn-qtrimmed-fixed.fastq.gz invitro-SN15-003-R2-atrimmed-pn-qtrimmed-fixed.fastq.gz invitro-SN15-003-atrimmed-pn-qtrimmed.log &

wait
