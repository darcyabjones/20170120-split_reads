OUT=04-trim_quality
DATA_DIR=data
SPLIT_DIR=03-split_by_reference
ADAPTER_DIR=01-trimmed_adapters

three_read1=AGATCGGAAGAGCACACGTCTGAACTCCAGTCACNNNNNNATCTCGTATGCCGTCTTCTGCTTG
three_read2=AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTAGATCTCGGTGGTCGCCGTATCATT
five_read1=AATGATACGGCGACCACCGAGATCTACACTCTTTCCCTACACGACGCTCTTCCGATCT
five_read2=GTTCGTCTTCTGCCGTATGCTCTANNNNNNCACTGACCTCAAGTCTGCACACGAGAAGGCTAG

function call_ca {
	cutadapt \
			--quality-cutoff=25 \
			--minimum-length 60 \
			--minimum-length 50 \
			-a "${three_read1}" \
			-A "${three_read2}" \
			-g "${five_read1}" \
			-G "${five_read2}" \
			-n 3 \
			-o "${OUT}/$3" \
			-p "${OUT}/$4" \
			"$1" "$2" \
			> "${OUT}/$5"
	exit
}


#call_ca ${SPLIT_DIR}/inplanta-3KO-020-R1-atrimmed-unmapped.fastq.gz ${SPLIT_DIR}/inplanta-3KO-020-R2-atrimmed-unmapped.fastq.gz inplanta-3KO-020-R1-atrimmed-unmapped-qtrimmed.fastq.gz inplanta-3KO-020-R2-atrimmed-unmapped-qtrimmed.fastq.gz inplanta-3KO-020-atrimmed-unmapped-qtrimmed.log &
#call_ca ${SPLIT_DIR}/inplanta-3KO-021-R1-atrimmed-unmapped.fastq.gz ${SPLIT_DIR}/inplanta-3KO-021-R2-atrimmed-unmapped.fastq.gz inplanta-3KO-021-R1-atrimmed-unmapped-qtrimmed.fastq.gz inplanta-3KO-021-R2-atrimmed-unmapped-qtrimmed.fastq.gz inplanta-3KO-021-atrimmed-unmapped-qtrimmed.log &
#call_ca ${SPLIT_DIR}/inplanta-3KO-022-R1-atrimmed-unmapped.fastq.gz ${SPLIT_DIR}/inplanta-3KO-022-R2-atrimmed-unmapped.fastq.gz inplanta-3KO-022-R1-atrimmed-unmapped-qtrimmed.fastq.gz inplanta-3KO-022-R2-atrimmed-unmapped-qtrimmed.fastq.gz inplanta-3KO-022-atrimmed-unmapped-qtrimmed.log &
#call_ca ${SPLIT_DIR}/inplanta-3KO-023-R1-atrimmed-unmapped.fastq.gz ${SPLIT_DIR}/inplanta-3KO-023-R2-atrimmed-unmapped.fastq.gz inplanta-3KO-023-R1-atrimmed-unmapped-qtrimmed.fastq.gz inplanta-3KO-023-R2-atrimmed-unmapped-qtrimmed.fastq.gz inplanta-3KO-023-atrimmed-unmapped-qtrimmed.log &
#call_ca ${SPLIT_DIR}/inplanta-Pf2-024-R1-atrimmed-unmapped.fastq.gz ${SPLIT_DIR}/inplanta-Pf2-024-R2-atrimmed-unmapped.fastq.gz inplanta-Pf2-024-R1-atrimmed-unmapped-qtrimmed.fastq.gz inplanta-Pf2-024-R2-atrimmed-unmapped-qtrimmed.fastq.gz inplanta-Pf2-024-atrimmed-unmapped-qtrimmed.log &
#call_ca ${SPLIT_DIR}/inplanta-Pf2-026-R1-atrimmed-unmapped.fastq.gz ${SPLIT_DIR}/inplanta-Pf2-026-R2-atrimmed-unmapped.fastq.gz inplanta-Pf2-026-R1-atrimmed-unmapped-qtrimmed.fastq.gz inplanta-Pf2-026-R2-atrimmed-unmapped-qtrimmed.fastq.gz inplanta-Pf2-026-atrimmed-unmapped-qtrimmed.log &
#call_ca ${SPLIT_DIR}/inplanta-Pf2-025-R1-atrimmed-unmapped.fastq.gz ${SPLIT_DIR}/inplanta-Pf2-025-R2-atrimmed-unmapped.fastq.gz inplanta-Pf2-025-R1-atrimmed-unmapped-qtrimmed.fastq.gz inplanta-Pf2-025-R2-atrimmed-unmapped-qtrimmed.fastq.gz inplanta-Pf2-025-atrimmed-unmapped-qtrimmed.log &
#call_ca ${SPLIT_DIR}/inplanta-Pf2-027-R1-atrimmed-unmapped.fastq.gz ${SPLIT_DIR}/inplanta-Pf2-027-R2-atrimmed-unmapped.fastq.gz inplanta-Pf2-027-R1-atrimmed-unmapped-qtrimmed.fastq.gz inplanta-Pf2-027-R2-atrimmed-unmapped-qtrimmed.fastq.gz inplanta-Pf2-027-atrimmed-unmapped-qtrimmed.log &
#call_ca ${SPLIT_DIR}/inplanta-SN15-016-R1-atrimmed-unmapped.fastq.gz ${SPLIT_DIR}/inplanta-SN15-016-R2-atrimmed-unmapped.fastq.gz inplanta-SN15-016-R1-atrimmed-unmapped-qtrimmed.fastq.gz inplanta-SN15-016-R2-atrimmed-unmapped-qtrimmed.fastq.gz inplanta-SN15-016-atrimmed-unmapped-qtrimmed.log &
#call_ca ${SPLIT_DIR}/inplanta-SN15-017-R1-atrimmed-unmapped.fastq.gz ${SPLIT_DIR}/inplanta-SN15-017-R2-atrimmed-unmapped.fastq.gz inplanta-SN15-017-R1-atrimmed-unmapped-qtrimmed.fastq.gz inplanta-SN15-017-R2-atrimmed-unmapped-qtrimmed.fastq.gz inplanta-SN15-017-atrimmed-unmapped-qtrimmed.log &
#call_ca ${SPLIT_DIR}/inplanta-SN15-018-R1-atrimmed-unmapped.fastq.gz ${SPLIT_DIR}/inplanta-SN15-018-R2-atrimmed-unmapped.fastq.gz inplanta-SN15-018-R1-atrimmed-unmapped-qtrimmed.fastq.gz inplanta-SN15-018-R2-atrimmed-unmapped-qtrimmed.fastq.gz inplanta-SN15-018-atrimmed-unmapped-qtrimmed.log &
#call_ca ${SPLIT_DIR}/inplanta-SN15-019-R1-atrimmed-unmapped.fastq.gz ${SPLIT_DIR}/inplanta-SN15-019-R2-atrimmed-unmapped.fastq.gz inplanta-SN15-019-R1-atrimmed-unmapped-qtrimmed.fastq.gz inplanta-SN15-019-R2-atrimmed-unmapped-qtrimmed.fastq.gz inplanta-SN15-019-atrimmed-unmapped-qtrimmed.log &

#call_ca ${SPLIT_DIR}/inplanta-3KO-020-R1-atrimmed-ta.fastq.gz ${SPLIT_DIR}/inplanta-3KO-020-R2-atrimmed-ta.fastq.gz inplanta-3KO-020-R1-atrimmed-ta-qtrimmed.fastq.gz inplanta-3KO-020-R2-atrimmed-ta-qtrimmed.fastq.gz inplanta-3KO-020-atrimmed-ta-qtrimmed.log &
#call_ca ${SPLIT_DIR}/inplanta-3KO-021-R1-atrimmed-ta.fastq.gz ${SPLIT_DIR}/inplanta-3KO-021-R2-atrimmed-ta.fastq.gz inplanta-3KO-021-R1-atrimmed-ta-qtrimmed.fastq.gz inplanta-3KO-021-R2-atrimmed-ta-qtrimmed.fastq.gz inplanta-3KO-021-atrimmed-ta-qtrimmed.log &
#call_ca ${SPLIT_DIR}/inplanta-3KO-022-R1-atrimmed-ta.fastq.gz ${SPLIT_DIR}/inplanta-3KO-022-R2-atrimmed-ta.fastq.gz inplanta-3KO-022-R1-atrimmed-ta-qtrimmed.fastq.gz inplanta-3KO-022-R2-atrimmed-ta-qtrimmed.fastq.gz inplanta-3KO-022-atrimmed-ta-qtrimmed.log &
#call_ca ${SPLIT_DIR}/inplanta-3KO-023-R1-atrimmed-ta.fastq.gz ${SPLIT_DIR}/inplanta-3KO-023-R2-atrimmed-ta.fastq.gz inplanta-3KO-023-R1-atrimmed-ta-qtrimmed.fastq.gz inplanta-3KO-023-R2-atrimmed-ta-qtrimmed.fastq.gz inplanta-3KO-023-atrimmed-ta-qtrimmed.log &
#call_ca ${SPLIT_DIR}/inplanta-Pf2-024-R1-atrimmed-ta.fastq.gz ${SPLIT_DIR}/inplanta-Pf2-024-R2-atrimmed-ta.fastq.gz inplanta-Pf2-024-R1-atrimmed-ta-qtrimmed.fastq.gz inplanta-Pf2-024-R2-atrimmed-ta-qtrimmed.fastq.gz inplanta-Pf2-024-atrimmed-ta-qtrimmed.log &
#call_ca ${SPLIT_DIR}/inplanta-Pf2-025-R1-atrimmed-ta.fastq.gz ${SPLIT_DIR}/inplanta-Pf2-025-R2-atrimmed-ta.fastq.gz inplanta-Pf2-025-R1-atrimmed-ta-qtrimmed.fastq.gz inplanta-Pf2-025-R2-atrimmed-ta-qtrimmed.fastq.gz inplanta-Pf2-025-atrimmed-ta-qtrimmed.log &
#call_ca ${SPLIT_DIR}/inplanta-Pf2-026-R1-atrimmed-ta.fastq.gz ${SPLIT_DIR}/inplanta-Pf2-026-R2-atrimmed-ta.fastq.gz inplanta-Pf2-026-R1-atrimmed-ta-qtrimmed.fastq.gz inplanta-Pf2-026-R2-atrimmed-ta-qtrimmed.fastq.gz inplanta-Pf2-026-atrimmed-ta-qtrimmed.log &
#call_ca ${SPLIT_DIR}/inplanta-Pf2-027-R1-atrimmed-ta.fastq.gz ${SPLIT_DIR}/inplanta-Pf2-027-R2-atrimmed-ta.fastq.gz inplanta-Pf2-027-R1-atrimmed-ta-qtrimmed.fastq.gz inplanta-Pf2-027-R2-atrimmed-ta-qtrimmed.fastq.gz inplanta-Pf2-027-atrimmed-ta-qtrimmed.log &
#call_ca ${SPLIT_DIR}/inplanta-SN15-016-R1-atrimmed-ta.fastq.gz ${SPLIT_DIR}/inplanta-SN15-016-R2-atrimmed-ta.fastq.gz inplanta-SN15-016-R1-atrimmed-ta-qtrimmed.fastq.gz inplanta-SN15-016-R2-atrimmed-ta-qtrimmed.fastq.gz inplanta-SN15-016-atrimmed-ta-qtrimmed.log &
#call_ca ${SPLIT_DIR}/inplanta-SN15-017-R1-atrimmed-ta.fastq.gz ${SPLIT_DIR}/inplanta-SN15-017-R2-atrimmed-ta.fastq.gz inplanta-SN15-017-R1-atrimmed-ta-qtrimmed.fastq.gz inplanta-SN15-017-R2-atrimmed-ta-qtrimmed.fastq.gz inplanta-SN15-017-atrimmed-ta-qtrimmed.log &
#call_ca ${SPLIT_DIR}/inplanta-SN15-018-R1-atrimmed-ta.fastq.gz ${SPLIT_DIR}/inplanta-SN15-018-R2-atrimmed-ta.fastq.gz inplanta-SN15-018-R1-atrimmed-ta-qtrimmed.fastq.gz inplanta-SN15-018-R2-atrimmed-ta-qtrimmed.fastq.gz inplanta-SN15-018-atrimmed-ta-qtrimmed.log &
#call_ca ${SPLIT_DIR}/inplanta-SN15-019-R1-atrimmed-ta.fastq.gz ${SPLIT_DIR}/inplanta-SN15-019-R2-atrimmed-ta.fastq.gz inplanta-SN15-019-R1-atrimmed-ta-qtrimmed.fastq.gz inplanta-SN15-019-R2-atrimmed-ta-qtrimmed.fastq.gz inplanta-SN15-019-atrimmed-ta-qtrimmed.log &
#call_ca ${ADAPTER_DIR}/inplanta-Tween-012-R1-atrimmed.fastq.gz ${ADAPTER_DIR}/inplanta-Tween-012-R2-atrimmed.fastq.gz inplanta-Tween-012-R1-atrimmed-ta-qtrimmed.fastq.gz inplanta-Tween-012-R2-atrimmed-ta-qtrimmed.fastq.gz inplanta-Tween-012-atrimmed-ta-qtrimmed.log &
#call_ca ${ADAPTER_DIR}/inplanta-Tween-013-R1-atrimmed.fastq.gz ${ADAPTER_DIR}/inplanta-Tween-013-R2-atrimmed.fastq.gz inplanta-Tween-013-R1-atrimmed-ta-qtrimmed.fastq.gz inplanta-Tween-013-R2-atrimmed-ta-qtrimmed.fastq.gz inplanta-Tween-013-atrimmed-ta-qtrimmed.log &
#call_ca ${ADAPTER_DIR}/inplanta-Tween-014-R1-atrimmed.fastq.gz ${ADAPTER_DIR}/inplanta-Tween-014-R2-atrimmed.fastq.gz inplanta-Tween-014-R1-atrimmed-ta-qtrimmed.fastq.gz inplanta-Tween-014-R2-atrimmed-ta-qtrimmed.fastq.gz inplanta-Tween-014-atrimmed-ta-qtrimmed.log &
#call_ca ${ADAPTER_DIR}/inplanta-Tween-015-R1-atrimmed.fastq.gz ${ADAPTER_DIR}/inplanta-Tween-015-R2-atrimmed.fastq.gz inplanta-Tween-015-R1-atrimmed-ta-qtrimmed.fastq.gz inplanta-Tween-015-R2-atrimmed-ta-qtrimmed.fastq.gz inplanta-Tween-015-atrimmed-ta-qtrimmed.log &

#call_ca ${SPLIT_DIR}/inplanta-3KO-020-R1-atrimmed-pn.fastq.gz ${SPLIT_DIR}/inplanta-3KO-020-R2-atrimmed-pn.fastq.gz inplanta-3KO-020-R1-atrimmed-pn-qtrimmed.fastq.gz inplanta-3KO-020-R2-atrimmed-pn-qtrimmed.fastq.gz inplanta-3KO-020-atrimmed-pn-qtrimmed.log &
#call_ca ${SPLIT_DIR}/inplanta-3KO-021-R1-atrimmed-pn.fastq.gz ${SPLIT_DIR}/inplanta-3KO-021-R2-atrimmed-pn.fastq.gz inplanta-3KO-021-R1-atrimmed-pn-qtrimmed.fastq.gz inplanta-3KO-021-R2-atrimmed-pn-qtrimmed.fastq.gz inplanta-3KO-021-atrimmed-pn-qtrimmed.log &
#call_ca ${SPLIT_DIR}/inplanta-3KO-022-R1-atrimmed-pn.fastq.gz ${SPLIT_DIR}/inplanta-3KO-022-R2-atrimmed-pn.fastq.gz inplanta-3KO-022-R1-atrimmed-pn-qtrimmed.fastq.gz inplanta-3KO-022-R2-atrimmed-pn-qtrimmed.fastq.gz inplanta-3KO-022-atrimmed-pn-qtrimmed.log &
#call_ca ${SPLIT_DIR}/inplanta-3KO-023-R1-atrimmed-pn.fastq.gz ${SPLIT_DIR}/inplanta-3KO-023-R2-atrimmed-pn.fastq.gz inplanta-3KO-023-R1-atrimmed-pn-qtrimmed.fastq.gz inplanta-3KO-023-R2-atrimmed-pn-qtrimmed.fastq.gz inplanta-3KO-023-atrimmed-pn-qtrimmed.log &
#call_ca ${SPLIT_DIR}/inplanta-Pf2-024-R1-atrimmed-pn.fastq.gz ${SPLIT_DIR}/inplanta-Pf2-024-R2-atrimmed-pn.fastq.gz inplanta-Pf2-024-R1-atrimmed-pn-qtrimmed.fastq.gz inplanta-Pf2-024-R2-atrimmed-pn-qtrimmed.fastq.gz inplanta-Pf2-024-atrimmed-pn-qtrimmed.log &
#call_ca ${SPLIT_DIR}/inplanta-Pf2-025-R1-atrimmed-pn.fastq.gz ${SPLIT_DIR}/inplanta-Pf2-025-R2-atrimmed-pn.fastq.gz inplanta-Pf2-025-R1-atrimmed-pn-qtrimmed.fastq.gz inplanta-Pf2-025-R2-atrimmed-pn-qtrimmed.fastq.gz inplanta-Pf2-025-atrimmed-pn-qtrimmed.log &
#call_ca ${SPLIT_DIR}/inplanta-Pf2-026-R1-atrimmed-pn.fastq.gz ${SPLIT_DIR}/inplanta-Pf2-026-R2-atrimmed-pn.fastq.gz inplanta-Pf2-026-R1-atrimmed-pn-qtrimmed.fastq.gz inplanta-Pf2-026-R2-atrimmed-pn-qtrimmed.fastq.gz inplanta-Pf2-026-atrimmed-pn-qtrimmed.log &
#call_ca ${SPLIT_DIR}/inplanta-Pf2-027-R1-atrimmed-pn.fastq.gz ${SPLIT_DIR}/inplanta-Pf2-027-R2-atrimmed-pn.fastq.gz inplanta-Pf2-027-R1-atrimmed-pn-qtrimmed.fastq.gz inplanta-Pf2-027-R2-atrimmed-pn-qtrimmed.fastq.gz inplanta-Pf2-027-atrimmed-pn-qtrimmed.log &
#call_ca ${SPLIT_DIR}/inplanta-SN15-016-R1-atrimmed-pn.fastq.gz ${SPLIT_DIR}/inplanta-SN15-016-R2-atrimmed-pn.fastq.gz inplanta-SN15-016-R1-atrimmed-pn-qtrimmed.fastq.gz inplanta-SN15-016-R2-atrimmed-pn-qtrimmed.fastq.gz inplanta-SN15-016-atrimmed-pn-qtrimmed.log &
#call_ca ${SPLIT_DIR}/inplanta-SN15-017-R1-atrimmed-pn.fastq.gz ${SPLIT_DIR}/inplanta-SN15-017-R2-atrimmed-pn.fastq.gz inplanta-SN15-017-R1-atrimmed-pn-qtrimmed.fastq.gz inplanta-SN15-017-R2-atrimmed-pn-qtrimmed.fastq.gz inplanta-SN15-017-atrimmed-pn-qtrimmed.log &
#call_ca ${SPLIT_DIR}/inplanta-SN15-018-R1-atrimmed-pn.fastq.gz ${SPLIT_DIR}/inplanta-SN15-018-R2-atrimmed-pn.fastq.gz inplanta-SN15-018-R1-atrimmed-pn-qtrimmed.fastq.gz inplanta-SN15-018-R2-atrimmed-pn-qtrimmed.fastq.gz inplanta-SN15-018-atrimmed-pn-qtrimmed.log &
#call_ca ${SPLIT_DIR}/inplanta-SN15-019-R1-atrimmed-pn.fastq.gz ${SPLIT_DIR}/inplanta-SN15-019-R2-atrimmed-pn.fastq.gz inplanta-SN15-019-R1-atrimmed-pn-qtrimmed.fastq.gz inplanta-SN15-019-R2-atrimmed-pn-qtrimmed.fastq.gz inplanta-SN15-019-atrimmed-pn-qtrimmed.log &

call_ca ${ADAPTER_DIR}/invitro-3KO-004-R1-atrimmed.fastq.gz ${ADAPTER_DIR}/invitro-3KO-004-R2-atrimmed.fastq.gz invitro-3KO-004-R1-atrimmed-pn-qtrimmed.fastq.gz invitro-3KO-004-R2-atrimmed-pn-qtrimmed.fastq.gz invitro-3KO-004-atrimmed-pn-qtrimmed.log &
call_ca ${ADAPTER_DIR}/invitro-3KO-005-R1-atrimmed.fastq.gz ${ADAPTER_DIR}/invitro-3KO-005-R2-atrimmed.fastq.gz invitro-3KO-005-R1-atrimmed-pn-qtrimmed.fastq.gz invitro-3KO-005-R2-atrimmed-pn-qtrimmed.fastq.gz invitro-3KO-005-atrimmed-pn-qtrimmed.log &
call_ca ${ADAPTER_DIR}/invitro-3KO-006-R1-atrimmed.fastq.gz ${ADAPTER_DIR}/invitro-3KO-006-R2-atrimmed.fastq.gz invitro-3KO-006-R1-atrimmed-pn-qtrimmed.fastq.gz invitro-3KO-006-R2-atrimmed-pn-qtrimmed.fastq.gz invitro-3KO-006-atrimmed-pn-qtrimmed.log &
call_ca ${ADAPTER_DIR}/invitro-3KO-007-R1-atrimmed.fastq.gz ${ADAPTER_DIR}/invitro-3KO-007-R2-atrimmed.fastq.gz invitro-3KO-007-R1-atrimmed-pn-qtrimmed.fastq.gz invitro-3KO-007-R2-atrimmed-pn-qtrimmed.fastq.gz invitro-3KO-007-atrimmed-pn-qtrimmed.log &
call_ca ${ADAPTER_DIR}/invitro-Pf2-008-R1-atrimmed.fastq.gz ${ADAPTER_DIR}/invitro-Pf2-008-R2-atrimmed.fastq.gz invitro-Pf2-008-R1-atrimmed-pn-qtrimmed.fastq.gz invitro-Pf2-008-R2-atrimmed-pn-qtrimmed.fastq.gz invitro-Pf2-008-atrimmed-pn-qtrimmed.log &
call_ca ${ADAPTER_DIR}/invitro-Pf2-009-R1-atrimmed.fastq.gz ${ADAPTER_DIR}/invitro-Pf2-009-R2-atrimmed.fastq.gz invitro-Pf2-009-R1-atrimmed-pn-qtrimmed.fastq.gz invitro-Pf2-009-R2-atrimmed-pn-qtrimmed.fastq.gz invitro-Pf2-009-atrimmed-pn-qtrimmed.log &
call_ca ${ADAPTER_DIR}/invitro-Pf2-010-R1-atrimmed.fastq.gz ${ADAPTER_DIR}/invitro-Pf2-010-R2-atrimmed.fastq.gz invitro-Pf2-010-R1-atrimmed-pn-qtrimmed.fastq.gz invitro-Pf2-010-R2-atrimmed-pn-qtrimmed.fastq.gz invitro-Pf2-010-atrimmed-pn-qtrimmed.log &
call_ca ${ADAPTER_DIR}/invitro-Pf2-011-R1-atrimmed.fastq.gz ${ADAPTER_DIR}/invitro-Pf2-011-R2-atrimmed.fastq.gz invitro-Pf2-011-R1-atrimmed-pn-qtrimmed.fastq.gz invitro-Pf2-011-R2-atrimmed-pn-qtrimmed.fastq.gz invitro-Pf2-011-atrimmed-pn-qtrimmed.log &
call_ca ${ADAPTER_DIR}/invitro-SN15-000-R1-atrimmed.fastq.gz ${ADAPTER_DIR}/invitro-SN15-000-R2-atrimmed.fastq.gz invitro-SN15-000-R1-atrimmed-pn-qtrimmed.fastq.gz invitro-SN15-000-R2-atrimmed-pn-qtrimmed.fastq.gz invitro-SN15-000-atrimmed-pn-qtrimmed.log &
call_ca ${ADAPTER_DIR}/invitro-SN15-001-R1-atrimmed.fastq.gz ${ADAPTER_DIR}/invitro-SN15-001-R2-atrimmed.fastq.gz invitro-SN15-001-R1-atrimmed-pn-qtrimmed.fastq.gz invitro-SN15-001-R2-atrimmed-pn-qtrimmed.fastq.gz invitro-SN15-001-atrimmed-pn-qtrimmed.log &
call_ca ${ADAPTER_DIR}/invitro-SN15-002-R1-atrimmed.fastq.gz ${ADAPTER_DIR}/invitro-SN15-002-R2-atrimmed.fastq.gz invitro-SN15-002-R1-atrimmed-pn-qtrimmed.fastq.gz invitro-SN15-002-R2-atrimmed-pn-qtrimmed.fastq.gz invitro-SN15-002-atrimmed-pn-qtrimmed.log &
call_ca ${ADAPTER_DIR}/invitro-SN15-003-R1-atrimmed.fastq.gz ${ADAPTER_DIR}/invitro-SN15-003-R2-atrimmed.fastq.gz invitro-SN15-003-R1-atrimmed-pn-qtrimmed.fastq.gz invitro-SN15-003-R2-atrimmed-pn-qtrimmed.fastq.gz invitro-SN15-003-atrimmed-pn-qtrimmed.log &

wait
