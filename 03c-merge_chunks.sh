#!/usr/bin/env bash

cat 03-split_by_reference/inplanta-3KO-020-*-atrimmed-refstats.tsv > 03-split_by_reference/inplanta-3KO-020-atrimmed-refstats.tsv
cat 03-split_by_reference/inplanta-3KO-020-*-atrimmed-scafstats.tsv > 03-split_by_reference/inplanta-3KO-020-atrimmed-scafstats.tsv
cat 03-split_by_reference/inplanta-3KO-020-*-R1-atrimmed-pn-pre.fastq.gz > 03-split_by_reference/inplanta-3KO-020-R1-atrimmed-pn.fastq.gz
cat 03-split_by_reference/inplanta-3KO-020-*-R1-atrimmed-ta-pre.fastq.gz > 03-split_by_reference/inplanta-3KO-020-R1-atrimmed-ta.fastq.gz
cat 03-split_by_reference/inplanta-3KO-020-*-R1-atrimmed-unmapped-pre.fastq.gz > 03-split_by_reference/inplanta-3KO-020-R1-atrimmed-unmapped.fastq.gz
cat 03-split_by_reference/inplanta-3KO-020-*-R2-atrimmed-pn-pre.fastq.gz > 03-split_by_reference/inplanta-3KO-020-R2-atrimmed-pn.fastq.gz
cat 03-split_by_reference/inplanta-3KO-020-*-R2-atrimmed-ta-pre.fastq.gz > 03-split_by_reference/inplanta-3KO-020-R2-atrimmed-ta.fastq.gz
cat 03-split_by_reference/inplanta-3KO-020-*-R2-atrimmed-unmapped-pre.fastq.gz > 03-split_by_reference/inplanta-3KO-020-R2-atrimmed-unmapped.fastq.gz

cat 03-split_by_reference/inplanta-3KO-021-*-atrimmed-refstats.tsv > 03-split_by_reference/inplanta-3KO-021-atrimmed-refstats.tsv
cat 03-split_by_reference/inplanta-3KO-021-*-atrimmed-scafstats.tsv > 03-split_by_reference/inplanta-3KO-021-atrimmed-scafstats.tsv
cat 03-split_by_reference/inplanta-3KO-021-*-R1-atrimmed-pn-pre.fastq.gz > 03-split_by_reference/inplanta-3KO-021-R1-atrimmed-pn.fastq.gz
cat 03-split_by_reference/inplanta-3KO-021-*-R1-atrimmed-ta-pre.fastq.gz > 03-split_by_reference/inplanta-3KO-021-R1-atrimmed-ta.fastq.gz
cat 03-split_by_reference/inplanta-3KO-021-*-R1-atrimmed-unmapped-pre.fastq.gz > 03-split_by_reference/inplanta-3KO-021-R1-atrimmed-unmapped.fastq.gz
cat 03-split_by_reference/inplanta-3KO-021-*-R2-atrimmed-pn-pre.fastq.gz > 03-split_by_reference/inplanta-3KO-021-R2-atrimmed-pn.fastq.gz
cat 03-split_by_reference/inplanta-3KO-021-*-R2-atrimmed-ta-pre.fastq.gz > 03-split_by_reference/inplanta-3KO-021-R2-atrimmed-ta.fastq.gz
cat 03-split_by_reference/inplanta-3KO-021-*-R2-atrimmed-unmapped-pre.fastq.gz > 03-split_by_reference/inplanta-3KO-021-R2-atrimmed-unmapped.fastq.gz

cat 03-split_by_reference/inplanta-3KO-022-*-atrimmed-refstats.tsv > 03-split_by_reference/inplanta-3KO-022-atrimmed-refstats.tsv
cat 03-split_by_reference/inplanta-3KO-022-*-atrimmed-scafstats.tsv > 03-split_by_reference/inplanta-3KO-022-atrimmed-scafstats.tsv
cat 03-split_by_reference/inplanta-3KO-022-*-R1-atrimmed-pn-pre.fastq.gz > 03-split_by_reference/inplanta-3KO-022-R1-atrimmed-pn.fastq.gz
cat 03-split_by_reference/inplanta-3KO-022-*-R1-atrimmed-ta-pre.fastq.gz > 03-split_by_reference/inplanta-3KO-022-R1-atrimmed-ta.fastq.gz
cat 03-split_by_reference/inplanta-3KO-022-*-R1-atrimmed-unmapped-pre.fastq.gz > 03-split_by_reference/inplanta-3KO-022-R1-atrimmed-unmapped.fastq.gz
cat 03-split_by_reference/inplanta-3KO-022-*-R2-atrimmed-pn-pre.fastq.gz > 03-split_by_reference/inplanta-3KO-022-R2-atrimmed-pn.fastq.gz
cat 03-split_by_reference/inplanta-3KO-022-*-R2-atrimmed-ta-pre.fastq.gz > 03-split_by_reference/inplanta-3KO-022-R2-atrimmed-ta.fastq.gz
cat 03-split_by_reference/inplanta-3KO-022-*-R2-atrimmed-unmapped-pre.fastq.gz > 03-split_by_reference/inplanta-3KO-022-R2-atrimmed-unmapped.fastq.gz

cat 03-split_by_reference/inplanta-3KO-023-*-atrimmed-refstats.tsv > 03-split_by_reference/inplanta-3KO-023-atrimmed-refstats.tsv
cat 03-split_by_reference/inplanta-3KO-023-*-atrimmed-scafstats.tsv > 03-split_by_reference/inplanta-3KO-023-atrimmed-scafstats.tsv
cat 03-split_by_reference/inplanta-3KO-023-*-R1-atrimmed-pn-pre.fastq.gz > 03-split_by_reference/inplanta-3KO-023-R1-atrimmed-pn.fastq.gz
cat 03-split_by_reference/inplanta-3KO-023-*-R1-atrimmed-ta-pre.fastq.gz > 03-split_by_reference/inplanta-3KO-023-R1-atrimmed-ta.fastq.gz
cat 03-split_by_reference/inplanta-3KO-023-*-R1-atrimmed-unmapped-pre.fastq.gz > 03-split_by_reference/inplanta-3KO-023-R1-atrimmed-unmapped.fastq.gz
cat 03-split_by_reference/inplanta-3KO-023-*-R2-atrimmed-pn-pre.fastq.gz > 03-split_by_reference/inplanta-3KO-023-R2-atrimmed-pn.fastq.gz
cat 03-split_by_reference/inplanta-3KO-023-*-R2-atrimmed-ta-pre.fastq.gz > 03-split_by_reference/inplanta-3KO-023-R2-atrimmed-ta.fastq.gz
cat 03-split_by_reference/inplanta-3KO-023-*-R2-atrimmed-unmapped-pre.fastq.gz > 03-split_by_reference/inplanta-3KO-023-R2-atrimmed-unmapped.fastq.gz

cat 03-split_by_reference/inplanta-Pf2-024-*-atrimmed-refstats.tsv > 03-split_by_reference/inplanta-Pf2-024-atrimmed-refstats.tsv
cat 03-split_by_reference/inplanta-Pf2-024-*-atrimmed-scafstats.tsv > 03-split_by_reference/inplanta-Pf2-024-atrimmed-scafstats.tsv
cat 03-split_by_reference/inplanta-Pf2-024-*-R1-atrimmed-pn-pre.fastq.gz > 03-split_by_reference/inplanta-Pf2-024-R1-atrimmed-pn.fastq.gz
cat 03-split_by_reference/inplanta-Pf2-024-*-R1-atrimmed-ta-pre.fastq.gz > 03-split_by_reference/inplanta-Pf2-024-R1-atrimmed-ta.fastq.gz
cat 03-split_by_reference/inplanta-Pf2-024-*-R1-atrimmed-unmapped-pre.fastq.gz > 03-split_by_reference/inplanta-Pf2-024-R1-atrimmed-unmapped.fastq.gz
cat 03-split_by_reference/inplanta-Pf2-024-*-R2-atrimmed-pn-pre.fastq.gz > 03-split_by_reference/inplanta-Pf2-024-R2-atrimmed-pn.fastq.gz
cat 03-split_by_reference/inplanta-Pf2-024-*-R2-atrimmed-ta-pre.fastq.gz > 03-split_by_reference/inplanta-Pf2-024-R2-atrimmed-ta.fastq.gz
cat 03-split_by_reference/inplanta-Pf2-024-*-R2-atrimmed-unmapped-pre.fastq.gz > 03-split_by_reference/inplanta-Pf2-024-R2-atrimmed-unmapped.fastq.gz

cat 03-split_by_reference/inplanta-Pf2-025-*-atrimmed-refstats.tsv > 03-split_by_reference/inplanta-Pf2-025-atrimmed-refstats.tsv
cat 03-split_by_reference/inplanta-Pf2-025-*-atrimmed-scafstats.tsv > 03-split_by_reference/inplanta-Pf2-025-atrimmed-scafstats.tsv
cat 03-split_by_reference/inplanta-Pf2-025-*-R1-atrimmed-pn-pre.fastq.gz > 03-split_by_reference/inplanta-Pf2-025-R1-atrimmed-pn.fastq.gz
cat 03-split_by_reference/inplanta-Pf2-025-*-R1-atrimmed-ta-pre.fastq.gz > 03-split_by_reference/inplanta-Pf2-025-R1-atrimmed-ta.fastq.gz
cat 03-split_by_reference/inplanta-Pf2-025-*-R1-atrimmed-unmapped-pre.fastq.gz > 03-split_by_reference/inplanta-Pf2-025-R1-atrimmed-unmapped.fastq.gz
cat 03-split_by_reference/inplanta-Pf2-025-*-R2-atrimmed-pn-pre.fastq.gz > 03-split_by_reference/inplanta-Pf2-025-R2-atrimmed-pn.fastq.gz
cat 03-split_by_reference/inplanta-Pf2-025-*-R2-atrimmed-ta-pre.fastq.gz > 03-split_by_reference/inplanta-Pf2-025-R2-atrimmed-ta.fastq.gz
cat 03-split_by_reference/inplanta-Pf2-025-*-R2-atrimmed-unmapped-pre.fastq.gz > 03-split_by_reference/inplanta-Pf2-025-R2-atrimmed-unmapped.fastq.gz

cat 03-split_by_reference/inplanta-Pf2-026-*-atrimmed-refstats.tsv > 03-split_by_reference/inplanta-Pf2-026-atrimmed-refstats.tsv
cat 03-split_by_reference/inplanta-Pf2-026-*-atrimmed-scafstats.tsv > 03-split_by_reference/inplanta-Pf2-026-atrimmed-scafstats.tsv
cat 03-split_by_reference/inplanta-Pf2-026-*-R1-atrimmed-pn-pre.fastq.gz > 03-split_by_reference/inplanta-Pf2-026-R1-atrimmed-pn.fastq.gz
cat 03-split_by_reference/inplanta-Pf2-026-*-R1-atrimmed-ta-pre.fastq.gz > 03-split_by_reference/inplanta-Pf2-026-R1-atrimmed-ta.fastq.gz
cat 03-split_by_reference/inplanta-Pf2-026-*-R1-atrimmed-unmapped-pre.fastq.gz > 03-split_by_reference/inplanta-Pf2-026-R1-atrimmed-unmapped.fastq.gz
cat 03-split_by_reference/inplanta-Pf2-026-*-R2-atrimmed-pn-pre.fastq.gz > 03-split_by_reference/inplanta-Pf2-026-R2-atrimmed-pn.fastq.gz
cat 03-split_by_reference/inplanta-Pf2-026-*-R2-atrimmed-ta-pre.fastq.gz > 03-split_by_reference/inplanta-Pf2-026-R2-atrimmed-ta.fastq.gz
cat 03-split_by_reference/inplanta-Pf2-026-*-R2-atrimmed-unmapped-pre.fastq.gz > 03-split_by_reference/inplanta-Pf2-026-R2-atrimmed-unmapped.fastq.gz

cat 03-split_by_reference/inplanta-Pf2-027-*-atrimmed-refstats.tsv > 03-split_by_reference/inplanta-Pf2-027-atrimmed-refstats.tsv
cat 03-split_by_reference/inplanta-Pf2-027-*-atrimmed-scafstats.tsv > 03-split_by_reference/inplanta-Pf2-027-atrimmed-scafstats.tsv
cat 03-split_by_reference/inplanta-Pf2-027-*-R1-atrimmed-pn-pre.fastq.gz > 03-split_by_reference/inplanta-Pf2-027-R1-atrimmed-pn.fastq.gz
cat 03-split_by_reference/inplanta-Pf2-027-*-R1-atrimmed-ta-pre.fastq.gz > 03-split_by_reference/inplanta-Pf2-027-R1-atrimmed-ta.fastq.gz
cat 03-split_by_reference/inplanta-Pf2-027-*-R1-atrimmed-unmapped-pre.fastq.gz > 03-split_by_reference/inplanta-Pf2-027-R1-atrimmed-unmapped.fastq.gz
cat 03-split_by_reference/inplanta-Pf2-027-*-R2-atrimmed-pn-pre.fastq.gz > 03-split_by_reference/inplanta-Pf2-027-R2-atrimmed-pn.fastq.gz
cat 03-split_by_reference/inplanta-Pf2-027-*-R2-atrimmed-ta-pre.fastq.gz > 03-split_by_reference/inplanta-Pf2-027-R2-atrimmed-ta.fastq.gz
cat 03-split_by_reference/inplanta-Pf2-027-*-R2-atrimmed-unmapped-pre.fastq.gz > 03-split_by_reference/inplanta-Pf2-027-R2-atrimmed-unmapped.fastq.gz

cat 03-split_by_reference/inplanta-SN15-016-*-atrimmed-refstats.tsv > 03-split_by_reference/inplanta-SN15-016-atrimmed-refstats.tsv
cat 03-split_by_reference/inplanta-SN15-016-*-atrimmed-scafstats.tsv > 03-split_by_reference/inplanta-SN15-016-atrimmed-scafstats.tsv
cat 03-split_by_reference/inplanta-SN15-016-*-R1-atrimmed-pn-pre.fastq.gz > 03-split_by_reference/inplanta-SN15-016-R1-atrimmed-pn.fastq.gz
cat 03-split_by_reference/inplanta-SN15-016-*-R1-atrimmed-ta-pre.fastq.gz > 03-split_by_reference/inplanta-SN15-016-R1-atrimmed-ta.fastq.gz
cat 03-split_by_reference/inplanta-SN15-016-*-R1-atrimmed-unmapped-pre.fastq.gz > 03-split_by_reference/inplanta-SN15-016-R1-atrimmed-unmapped.fastq.gz
cat 03-split_by_reference/inplanta-SN15-016-*-R2-atrimmed-pn-pre.fastq.gz > 03-split_by_reference/inplanta-SN15-016-R2-atrimmed-pn.fastq.gz
cat 03-split_by_reference/inplanta-SN15-016-*-R2-atrimmed-ta-pre.fastq.gz > 03-split_by_reference/inplanta-SN15-016-R2-atrimmed-ta.fastq.gz
cat 03-split_by_reference/inplanta-SN15-016-*-R2-atrimmed-unmapped-pre.fastq.gz > 03-split_by_reference/inplanta-SN15-016-R2-atrimmed-unmapped.fastq.gz

cat 03-split_by_reference/inplanta-SN15-017-*-atrimmed-refstats.tsv > 03-split_by_reference/inplanta-SN15-017-atrimmed-refstats.tsv
cat 03-split_by_reference/inplanta-SN15-017-*-atrimmed-scafstats.tsv > 03-split_by_reference/inplanta-SN15-017-atrimmed-scafstats.tsv
cat 03-split_by_reference/inplanta-SN15-017-*-R1-atrimmed-pn-pre.fastq.gz > 03-split_by_reference/inplanta-SN15-017-R1-atrimmed-pn.fastq.gz
cat 03-split_by_reference/inplanta-SN15-017-*-R1-atrimmed-ta-pre.fastq.gz > 03-split_by_reference/inplanta-SN15-017-R1-atrimmed-ta.fastq.gz
cat 03-split_by_reference/inplanta-SN15-017-*-R1-atrimmed-unmapped-pre.fastq.gz > 03-split_by_reference/inplanta-SN15-017-R1-atrimmed-unmapped.fastq.gz
cat 03-split_by_reference/inplanta-SN15-017-*-R2-atrimmed-pn-pre.fastq.gz > 03-split_by_reference/inplanta-SN15-017-R2-atrimmed-pn.fastq.gz
cat 03-split_by_reference/inplanta-SN15-017-*-R2-atrimmed-ta-pre.fastq.gz > 03-split_by_reference/inplanta-SN15-017-R2-atrimmed-ta.fastq.gz
cat 03-split_by_reference/inplanta-SN15-017-*-R2-atrimmed-unmapped-pre.fastq.gz > 03-split_by_reference/inplanta-SN15-017-R2-atrimmed-unmapped.fastq.gz

cat 03-split_by_reference/inplanta-SN15-018-*-atrimmed-refstats.tsv > 03-split_by_reference/inplanta-SN15-018-atrimmed-refstats.tsv
cat 03-split_by_reference/inplanta-SN15-018-*-atrimmed-scafstats.tsv > 03-split_by_reference/inplanta-SN15-018-atrimmed-scafstats.tsv
cat 03-split_by_reference/inplanta-SN15-018-*-R1-atrimmed-pn-pre.fastq.gz > 03-split_by_reference/inplanta-SN15-018-R1-atrimmed-pn.fastq.gz
cat 03-split_by_reference/inplanta-SN15-018-*-R1-atrimmed-ta-pre.fastq.gz > 03-split_by_reference/inplanta-SN15-018-R1-atrimmed-ta.fastq.gz
cat 03-split_by_reference/inplanta-SN15-018-*-R1-atrimmed-unmapped-pre.fastq.gz > 03-split_by_reference/inplanta-SN15-018-R1-atrimmed-unmapped.fastq.gz
cat 03-split_by_reference/inplanta-SN15-018-*-R2-atrimmed-pn-pre.fastq.gz > 03-split_by_reference/inplanta-SN15-018-R2-atrimmed-pn.fastq.gz
cat 03-split_by_reference/inplanta-SN15-018-*-R2-atrimmed-ta-pre.fastq.gz > 03-split_by_reference/inplanta-SN15-018-R2-atrimmed-ta.fastq.gz
cat 03-split_by_reference/inplanta-SN15-018-*-R2-atrimmed-unmapped-pre.fastq.gz > 03-split_by_reference/inplanta-SN15-018-R2-atrimmed-unmapped.fastq.gz

cat 03-split_by_reference/inplanta-SN15-019-*-atrimmed-refstats.tsv > 03-split_by_reference/inplanta-SN15-019-atrimmed-refstats.tsv
cat 03-split_by_reference/inplanta-SN15-019-*-atrimmed-scafstats.tsv > 03-split_by_reference/inplanta-SN15-019-atrimmed-scafstats.tsv
cat 03-split_by_reference/inplanta-SN15-019-*-R1-atrimmed-pn-pre.fastq.gz > 03-split_by_reference/inplanta-SN15-019-R1-atrimmed-pn.fastq.gz
cat 03-split_by_reference/inplanta-SN15-019-*-R1-atrimmed-ta-pre.fastq.gz > 03-split_by_reference/inplanta-SN15-019-R1-atrimmed-ta.fastq.gz
cat 03-split_by_reference/inplanta-SN15-019-*-R1-atrimmed-unmapped-pre.fastq.gz > 03-split_by_reference/inplanta-SN15-019-R1-atrimmed-unmapped.fastq.gz
cat 03-split_by_reference/inplanta-SN15-019-*-R2-atrimmed-pn-pre.fastq.gz > 03-split_by_reference/inplanta-SN15-019-R2-atrimmed-pn.fastq.gz
cat 03-split_by_reference/inplanta-SN15-019-*-R2-atrimmed-ta-pre.fastq.gz > 03-split_by_reference/inplanta-SN15-019-R2-atrimmed-ta.fastq.gz
cat 03-split_by_reference/inplanta-SN15-019-*-R2-atrimmed-unmapped-pre.fastq.gz > 03-split_by_reference/inplanta-SN15-019-R2-atrimmed-unmapped.fastq.gz

cat 03-split_by_reference/inplanta-Tween-012-*-atrimmed-refstats.tsv > 03-split_by_reference/inplanta-Tween-012-atrimmed-refstats.tsv
cat 03-split_by_reference/inplanta-Tween-012-*-atrimmed-refstats.tsv > 03-split_by_reference/inplanta-Tween-012-atrimmed-refstats.tsv
cat 03-split_by_reference/inplanta-Tween-013-*-atrimmed-refstats.tsv > 03-split_by_reference/inplanta-Tween-013-atrimmed-refstats.tsv
cat 03-split_by_reference/inplanta-Tween-013-*-atrimmed-refstats.tsv > 03-split_by_reference/inplanta-Tween-013-atrimmed-refstats.tsv
cat 03-split_by_reference/inplanta-Tween-014-*-atrimmed-refstats.tsv > 03-split_by_reference/inplanta-Tween-014-atrimmed-refstats.tsv
cat 03-split_by_reference/inplanta-Tween-014-*-atrimmed-scafstats.tsv > 03-split_by_reference/inplanta-Tween-014-atrimmed-scafstats.tsv
cat 03-split_by_reference/inplanta-Tween-015-*-atrimmed-refstats.tsv > 03-split_by_reference/inplanta-Tween-015-atrimmed-refstats.tsv
cat 03-split_by_reference/inplanta-Tween-015-*-atrimmed-scafstats.tsv > 03-split_by_reference/inplanta-Tween-015-atrimmed-scafstats.tsv
