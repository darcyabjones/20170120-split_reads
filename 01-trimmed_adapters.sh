#!/usr/bin/env bash

ADAPTER_DIR=01-trimmed_adapters
DATA_DIR=data

three_read1=AGATCGGAAGAGCACACGTCTGAACTCCAGTCACNNNNNNATCTCGTATGCCGTCTTCTGCTTG
three_read2=AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTAGATCTCGGTGGTCGCCGTATCATT
five_read1=AATGATACGGCGACCACCGAGATCTACACTCTTTCCCTACACGACGCTCTTCCGATCT
five_read2=GTTCGTCTTCTGCCGTATGCTCTANNNNNNCACTGACCTCAAGTCTGCACACGAGAAGGCTAG

### Sample names
# inplanta-3KO-020-R1.fastq.gz
# inplanta-3KO-021-R1.fastq.gz
# inplanta-3KO-022-R1.fastq.gz
# inplanta-3KO-023-R1.fastq.gz
# inplanta-Pf2-024-R1.fastq.gz
# inplanta-Pf2-025-R1.fastq.gz
# inplanta-Pf2-026-R1.fastq.gz
# inplanta-Pf2-027-R1.fastq.gz
# inplanta-SN15-016-R1.fastq.gz
# inplanta-SN15-017-R1.fastq.gz
# inplanta-SN15-018-R1.fastq.gz
# inplanta-SN15-019-R1.fastq.gz
# inplanta-Tween-012-R1.fastq.gz
# inplanta-Tween-013-R1.fastq.gz
# inplanta-Tween-014-R1.fastq.gz
# inplanta-Tween-015-R1.fastq.gz
# invitro-3KO-004-R1.fastq.gz
# invitro-3KO-005-R1.fastq.gz
# invitro-3KO-006-R1.fastq.gz
# invitro-3KO-007-R1.fastq.gz
# invitro-Pf2-008-R1.fastq.gz
# invitro-Pf2-009-R1.fastq.gz
# invitro-Pf2-010-R1.fastq.gz
# invitro-Pf2-011-R1.fastq.gz
# invitro-SN15-000-R1.fastq.gz
# invitro-SN15-001-R1.fastq.gz
# invitro-SN15-002-R1.fastq.gz
# invitro-SN15-003-R1.fastq.gz


# inplanta-3KO-020-R2.fastq.gz
# inplanta-3KO-021-R2.fastq.gz
# inplanta-3KO-022-R2.fastq.gz
# inplanta-3KO-023-R2.fastq.gz
# inplanta-Pf2-024-R2.fastq.gz
# inplanta-Pf2-025-R2.fastq.gz
# inplanta-Pf2-026-R2.fastq.gz
# inplanta-Pf2-027-R2.fastq.gz
# inplanta-SN15-016-R2.fastq.gz
# inplanta-SN15-017-R2.fastq.gz
# inplanta-SN15-018-R2.fastq.gz
# inplanta-SN15-019-R2.fastq.gz
# inplanta-Tween-012-R2.fastq.gz
# inplanta-Tween-013-R2.fastq.gz
# inplanta-Tween-014-R2.fastq.gz
# inplanta-Tween-015-R2.fastq.gz
# invitro-3KO-004-R2.fastq.gz
# invitro-3KO-005-R2.fastq.gz
# invitro-3KO-006-R2.fastq.gz
# invitro-3KO-007-R2.fastq.gz
# invitro-Pf2-008-R2.fastq.gz
# invitro-Pf2-009-R2.fastq.gz
# invitro-Pf2-010-R2.fastq.gz
# invitro-Pf2-011-R2.fastq.gz
# invitro-SN15-000-R2.fastq.gz
# invitro-SN15-001-R2.fastq.gz
# invitro-SN15-002-R2.fastq.gz
# invitro-SN15-003-R2.fastq.gz


function call_ca {
	cutadapt \
			--minimum-length 50 \
			-a "${three_read1}" \
			-A "${three_read2}" \
			-g "${five_read1}" \
			-G "${five_read2}" \
			-n 3 \
			-o "${ADAPTER_DIR}/$3" \
			-p "${ADAPTER_DIR}/$4" \
			"${DATA_DIR}/$1" "${DATA_DIR}/$2" \
			> "${ADAPTER_DIR}/$5"
	exit
}

#test
#call_ca inplanta-SN15-002-R1.fastq.gz inplanta-SN15-002-R2.fastq.gz inplanta-SN15-002-R1-atrimmed.fastq.gz inplanta-SN15-002-R2-atrimmed.fastq.gz inplanta-SN15-002-atrimmed.log

call_ca inplanta-3KO-020-R1.fastq.gz inplanta-3KO-020-R2.fastq.gz inplanta-3KO-020-R1-atrimmed.fastq.gz inplanta-3KO-020-R2-atrimmed.fastq.gz inplanta-3KO-020-atrimmed.log
call_ca inplanta-3KO-021-R1.fastq.gz inplanta-3KO-021-R2.fastq.gz inplanta-3KO-021-R1-atrimmed.fastq.gz inplanta-3KO-021-R2-atrimmed.fastq.gz inplanta-3KO-021-atrimmed.log
call_ca inplanta-3KO-022-R1.fastq.gz inplanta-3KO-022-R2.fastq.gz inplanta-3KO-022-R1-atrimmed.fastq.gz inplanta-3KO-022-R2-atrimmed.fastq.gz inplanta-3KO-022-atrimmed.log
call_ca inplanta-3KO-023-R1.fastq.gz inplanta-3KO-023-R2.fastq.gz inplanta-3KO-023-R1-atrimmed.fastq.gz inplanta-3KO-023-R2-atrimmed.fastq.gz inplanta-3KO-023-atrimmed.log
call_ca inplanta-Pf2-024-R1.fastq.gz inplanta-Pf2-024-R2.fastq.gz inplanta-Pf2-024-R1-atrimmed.fastq.gz inplanta-Pf2-024-R2-atrimmed.fastq.gz inplanta-Pf2-024-atrimmed.log
call_ca inplanta-Pf2-025-R1.fastq.gz inplanta-Pf2-025-R2.fastq.gz inplanta-Pf2-025-R1-atrimmed.fastq.gz inplanta-Pf2-025-R2-atrimmed.fastq.gz inplanta-Pf2-025-atrimmed.log
call_ca inplanta-Pf2-026-R1.fastq.gz inplanta-Pf2-026-R2.fastq.gz inplanta-Pf2-026-R1-atrimmed.fastq.gz inplanta-Pf2-026-R2-atrimmed.fastq.gz inplanta-Pf2-026-atrimmed.log
call_ca inplanta-Pf2-027-R1.fastq.gz inplanta-Pf2-027-R2.fastq.gz inplanta-Pf2-027-R1-atrimmed.fastq.gz inplanta-Pf2-027-R2-atrimmed.fastq.gz inplanta-Pf2-027-atrimmed.log
call_ca inplanta-SN15-016-R1.fastq.gz inplanta-SN15-016-R2.fastq.gz inplanta-SN15-016-R1-atrimmed.fastq.gz inplanta-SN15-016-R2-atrimmed.fastq.gz inplanta-SN15-016-atrimmed.log
call_ca inplanta-SN15-017-R1.fastq.gz inplanta-SN15-017-R2.fastq.gz inplanta-SN15-017-R1-atrimmed.fastq.gz inplanta-SN15-017-R2-atrimmed.fastq.gz inplanta-SN15-017-atrimmed.log
call_ca inplanta-SN15-018-R1.fastq.gz inplanta-SN15-018-R2.fastq.gz inplanta-SN15-018-R1-atrimmed.fastq.gz inplanta-SN15-018-R2-atrimmed.fastq.gz inplanta-SN15-018-atrimmed.log
call_ca inplanta-SN15-019-R1.fastq.gz inplanta-SN15-019-R2.fastq.gz inplanta-SN15-019-R1-atrimmed.fastq.gz inplanta-SN15-019-R2-atrimmed.fastq.gz inplanta-SN15-019-atrimmed.log
call_ca inplanta-Tween-012-R1.fastq.gz inplanta-Tween-012-R2.fastq.gz inplanta-Tween-012-R1-atrimmed.fastq.gz inplanta-Tween-012-R2-atrimmed.fastq.gz inplanta-Tween-012-atrimmed.log
call_ca inplanta-Tween-013-R1.fastq.gz inplanta-Tween-013-R2.fastq.gz inplanta-Tween-013-R1-atrimmed.fastq.gz inplanta-Tween-013-R2-atrimmed.fastq.gz inplanta-Tween-013-atrimmed.log
call_ca inplanta-Tween-014-R1.fastq.gz inplanta-Tween-014-R2.fastq.gz inplanta-Tween-014-R1-atrimmed.fastq.gz inplanta-Tween-014-R2-atrimmed.fastq.gz inplanta-Tween-014-atrimmed.log
call_ca inplanta-Tween-015-R1.fastq.gz inplanta-Tween-015-R2.fastq.gz inplanta-Tween-015-R1-atrimmed.fastq.gz inplanta-Tween-015-R2-atrimmed.fastq.gz inplanta-Tween-015-atrimmed.log
call_ca invitro-3KO-004-R1.fastq.gz invitro-3KO-004-R2.fastq.gz invitro-3KO-004-R1-atrimmed.fastq.gz invitro-3KO-004-R2-atrimmed.fastq.gz invitro-3KO-004-atrimmed.log
call_ca invitro-3KO-005-R1.fastq.gz invitro-3KO-005-R2.fastq.gz invitro-3KO-005-R1-atrimmed.fastq.gz invitro-3KO-005-R2-atrimmed.fastq.gz invitro-3KO-005-atrimmed.log
call_ca invitro-3KO-006-R1.fastq.gz invitro-3KO-006-R2.fastq.gz invitro-3KO-006-R1-atrimmed.fastq.gz invitro-3KO-006-R2-atrimmed.fastq.gz invitro-3KO-006-atrimmed.log
call_ca invitro-3KO-007-R1.fastq.gz invitro-3KO-007-R2.fastq.gz invitro-3KO-007-R1-atrimmed.fastq.gz invitro-3KO-007-R2-atrimmed.fastq.gz invitro-3KO-007-atrimmed.log
call_ca invitro-Pf2-008-R1.fastq.gz invitro-Pf2-008-R2.fastq.gz invitro-Pf2-008-R1-atrimmed.fastq.gz invitro-Pf2-008-R2-atrimmed.fastq.gz invitro-Pf2-008-atrimmed.log
call_ca invitro-Pf2-009-R1.fastq.gz invitro-Pf2-009-R2.fastq.gz invitro-Pf2-009-R1-atrimmed.fastq.gz invitro-Pf2-009-R2-atrimmed.fastq.gz invitro-Pf2-009-atrimmed.log
call_ca invitro-Pf2-010-R1.fastq.gz invitro-Pf2-010-R2.fastq.gz invitro-Pf2-010-R1-atrimmed.fastq.gz invitro-Pf2-010-R2-atrimmed.fastq.gz invitro-Pf2-010-atrimmed.log
call_ca invitro-Pf2-011-R1.fastq.gz invitro-Pf2-011-R2.fastq.gz invitro-Pf2-011-R1-atrimmed.fastq.gz invitro-Pf2-011-R2-atrimmed.fastq.gz invitro-Pf2-011-atrimmed.log
call_ca invitro-SN15-000-R1.fastq.gz invitro-SN15-000-R2.fastq.gz invitro-SN15-000-R1-atrimmed.fastq.gz invitro-SN15-000-R2-atrimmed.fastq.gz invitro-SN15-000-atrimmed.log
call_ca invitro-SN15-001-R1.fastq.gz invitro-SN15-001-R2.fastq.gz invitro-SN15-001-R1-atrimmed.fastq.gz invitro-SN15-001-R2-atrimmed.fastq.gz invitro-SN15-001-atrimmed.log
call_ca invitro-SN15-002-R1.fastq.gz invitro-SN15-002-R2.fastq.gz invitro-SN15-002-R1-atrimmed.fastq.gz invitro-SN15-002-R2-atrimmed.fastq.gz invitro-SN15-002-atrimmed.log
call_ca invitro-SN15-003-R1.fastq.gz invitro-SN15-003-R2.fastq.gz invitro-SN15-003-R1-atrimmed.fastq.gz invitro-SN15-003-R2-atrimmed.fastq.gz invitro-SN15-003-atrimmed.log
