
#!/usr/bin/env python3

from __future__ import print_function, division

program = "get_file_names"
version = "0.1.0"
author = "Darcy Jones"
date = "1 October 2015"
email = "darcy.ab.jones@gmail.com"
short_blurb = (
    'Splits a file into smaller files with x occurrences of a pattern'
    )
license = (
    '{program}-{version}\n'
    '{short_blurb}\n\n'
    'Copyright (C) {date},  {author}'
    '\n\n'
    'This program is free software: you can redistribute it and/or modify '
    'it under the terms of the GNU General Public License as published by '
    'the Free Software Foundation, either version 3 of the License, or '
    '(at your option) any later version.'
    '\n\n'
    'This program is distributed in the hope that it will be useful, '
    'but WITHOUT ANY WARRANTY; without even the implied warranty of '
    'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the '
    'GNU General Public License for more details.'
    '\n\n'
    'You should have received a copy of the GNU General Public License '
    'along with this program. If not, see <http://www.gnu.org/licenses/>.'
    )

license = license.format(**locals())

############################ Import all modules ##############################

import os
from os.path import split as psplit
from os.path import splitext as splitext
from os.path import join as pjoin
import argparse
import sys
from math import ceil

################################## Classes ###################################

# thanks to http://stackoverflow.com/questions/19966519/argparse-optional-stdin-argument


def main(infile, prefix, num_records, num_digits, verbose=False):

    if prefix is None:
        fname = psplit(infile.name)[1]
        fname_base = fname.split(".")[0][:-3]
        fname_read = fname.split(".")[0][-2:]
        fname_base += "-{{d}}-{}-atrimmed.fastq".format(fname_read)
        prefix = pjoin("02-split_by_reference", fname_base)

    total_records = int(infile.read().strip())
    num_records = int(num_records)

    num_files = int(ceil(total_records / num_records))

    files = [
        prefix.format(d = str(c).zfill(num_digits))
        for c in range(1, num_files + 1)
        ]
    print(" ".join(files))

    return

############################ Argument Handling ###############################

if __name__== '__main__':
    arg_parser = argparse.ArgumentParser(
      description=license,
      )
    arg_parser.add_argument(
        "-i", "--infile",
        default=sys.stdin,
        type=argparse.FileType('r'),
        help="Default is '-' (stdin)"
        )
    arg_parser.add_argument(
        "-n", "--num_records",
        dest='num_records',
        default=100,
        help=""
        )
    arg_parser.add_argument(
        "-d", "--num_digits",
        dest='num_digits',
        default=3,
        help=""
        )
    arg_parser.add_argument(
        "-p","--prefix",
        default=None,
        help=""
        )

    args = arg_parser.parse_args()

main(**args.__dict__)
