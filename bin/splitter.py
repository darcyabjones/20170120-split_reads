
#!/usr/bin/env python3

from __future__ import print_function

program = "splitter"
version = "0.1.0"
author = "Darcy Jones"
date = "1 October 2015"
email = "darcy.ab.jones@gmail.com"
short_blurb = (
    'Splits a file into smaller files with x occurrences of a pattern'
    )
license = (
    '{program}-{version}\n'
    '{short_blurb}\n\n'
    'Copyright (C) {date},  {author}'
    '\n\n'
    'This program is free software: you can redistribute it and/or modify '
    'it under the terms of the GNU General Public License as published by '
    'the Free Software Foundation, either version 3 of the License, or '
    '(at your option) any later version.'
    '\n\n'
    'This program is distributed in the hope that it will be useful, '
    'but WITHOUT ANY WARRANTY; without even the implied warranty of '
    'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the '
    'GNU General Public License for more details.'
    '\n\n'
    'You should have received a copy of the GNU General Public License '
    'along with this program. If not, see <http://www.gnu.org/licenses/>.'
    )

license = license.format(**locals())

############################ Import all modules ##############################

import os
from os.path import split as psplit
from os.path import splitext as splitext
import re
import argparse
import sys
from collections import defaultdict

################################## Classes ###################################


def main(infile, prefix, num_records, num_digits, pattern, no_write=False, verbose=False):

    num_records = int(num_records)
    start_regex = re.compile(pattern)
    chunk_num = 1

    if infile == sys.stdin:
        ext = ""
    else:
        ext = splitext(infile.name)[1]

    if "{d}" not in prefix:
        prefix += "-{d}" + ext

    filepaths = list()
    record_num = -1


    try:
        fp = prefix.format(d = str(chunk_num).zfill(num_digits))
        outhandle = open(fp, 'w')

        for line in infile:
            if start_regex.match(line) is not None:
                record_num += 1
                if record_num >= num_records:
                    outhandle.close()

                    chunk_num += 1
                    record_num = 0

                    fp = prefix.format(d = str(chunk_num).zfill(num_digits))
                    outhandle = open(fp, 'w')
                    filepaths.append(fp)

            outhandle.write(line)
    finally:
        outhandle.close()

    return

############################ Argument Handling ###############################

if __name__== '__main__':
    arg_parser = argparse.ArgumentParser(
      description=license,
      )
    arg_parser.add_argument(
        "-i", "--infile",
        default=sys.stdin,
        type=argparse.FileType('r'),
        help="Default is '-' (stdin)"
        )
    arg_parser.add_argument(
        "-n", "--num_records",
        dest='num_records',
        default=100,
        help=""
        )
    arg_parser.add_argument(
        "-d", "--num_digits",
        dest='num_digits',
        default=3,
        help=""
        )
    arg_parser.add_argument(
        "-p","--prefix",
        default='split',
        help=""
        )
    arg_parser.add_argument(
        "-r","--pattern",
        default="^>",
        help=""
        )

    args = arg_parser.parse_args()

main(**args.__dict__)
