CHUNK_DIR="02-chunkify_fastqs"
SPLIT_DIR="03-split_by_reference"
OLD_EXT="-atrimmed.fastq"

PN_GENOME_FILE=data/SN15v9_OM_nonewline.fasta
PN_GENOME_NAME=pn
TA_GENOME_FILE=data/Triticum_aestivum.TGACv1.dna_sm.toplevel.fa
TA_GENOME_NAME=ta


function call_bbsplit {
	bbsplit.sh \
		k=13 \
		untrim=t \
		ambiguous=all \
		path=${SPLIT_DIR}/index \
		build=1 \
		basename="${SPLIT_DIR}/${1%%${OLD_EXT}}-R#-atrimmed-%-pre.fastq.gz" \
		outu="${SPLIT_DIR}/${1%%${OLD_EXT}}-R#-atrimmed-unmapped-pre.fastq.gz" \
		in="${CHUNK_DIR}/$1" \
		in2="${CHUNK_DIR}/$2" \
		ambiguous2=best \
		maxindel=20000 \
		refstats=${SPLIT_DIR}/${1%%${OLD_EXT}}-atrimmed-refstats.tsv \
		scafstats=${SPLIT_DIR}/${1%%${OLD_EXT}}-atrimmed-scafstats.tsv
}


#call_bbsplit inplanta-3KO-020-001-R1-atrimmed.fastq inplanta-3KO-020-001-R2-atrimmed.fastq
#call_bbsplit inplanta-3KO-020-002-R1-atrimmed.fastq inplanta-3KO-020-002-R2-atrimmed.fastq
#call_bbsplit inplanta-3KO-020-003-R1-atrimmed.fastq inplanta-3KO-020-003-R2-atrimmed.fastq
#call_bbsplit inplanta-3KO-020-004-R1-atrimmed.fastq inplanta-3KO-020-004-R2-atrimmed.fastq
#call_bbsplit inplanta-3KO-020-005-R1-atrimmed.fastq inplanta-3KO-020-005-R2-atrimmed.fastq
#call_bbsplit inplanta-3KO-020-006-R1-atrimmed.fastq inplanta-3KO-020-006-R2-atrimmed.fastq
#call_bbsplit inplanta-3KO-020-007-R1-atrimmed.fastq inplanta-3KO-020-007-R2-atrimmed.fastq
#call_bbsplit inplanta-3KO-020-008-R1-atrimmed.fastq inplanta-3KO-020-008-R2-atrimmed.fastq
#call_bbsplit inplanta-3KO-021-001-R1-atrimmed.fastq inplanta-3KO-021-001-R2-atrimmed.fastq
#call_bbsplit inplanta-3KO-021-002-R1-atrimmed.fastq inplanta-3KO-021-002-R2-atrimmed.fastq
#call_bbsplit inplanta-3KO-021-003-R1-atrimmed.fastq inplanta-3KO-021-003-R2-atrimmed.fastq
#call_bbsplit inplanta-3KO-021-004-R1-atrimmed.fastq inplanta-3KO-021-004-R2-atrimmed.fastq
#call_bbsplit inplanta-3KO-021-005-R1-atrimmed.fastq inplanta-3KO-021-005-R2-atrimmed.fastq
#call_bbsplit inplanta-3KO-021-006-R1-atrimmed.fastq inplanta-3KO-021-006-R2-atrimmed.fastq
#call_bbsplit inplanta-3KO-021-007-R1-atrimmed.fastq inplanta-3KO-021-007-R2-atrimmed.fastq
#call_bbsplit inplanta-3KO-021-008-R1-atrimmed.fastq inplanta-3KO-021-008-R2-atrimmed.fastq
#call_bbsplit inplanta-3KO-022-001-R1-atrimmed.fastq inplanta-3KO-022-001-R2-atrimmed.fastq
#call_bbsplit inplanta-3KO-022-002-R1-atrimmed.fastq inplanta-3KO-022-002-R2-atrimmed.fastq
#call_bbsplit inplanta-3KO-022-003-R1-atrimmed.fastq inplanta-3KO-022-003-R2-atrimmed.fastq
#call_bbsplit inplanta-3KO-022-004-R1-atrimmed.fastq inplanta-3KO-022-004-R2-atrimmed.fastq
#call_bbsplit inplanta-3KO-022-005-R1-atrimmed.fastq inplanta-3KO-022-005-R2-atrimmed.fastq
#call_bbsplit inplanta-3KO-022-006-R1-atrimmed.fastq inplanta-3KO-022-006-R2-atrimmed.fastq
#call_bbsplit inplanta-3KO-022-007-R1-atrimmed.fastq inplanta-3KO-022-007-R2-atrimmed.fastq
#call_bbsplit inplanta-3KO-022-008-R1-atrimmed.fastq inplanta-3KO-022-008-R2-atrimmed.fastq
#call_bbsplit inplanta-3KO-023-001-R1-atrimmed.fastq inplanta-3KO-023-001-R2-atrimmed.fastq
#call_bbsplit inplanta-3KO-023-002-R1-atrimmed.fastq inplanta-3KO-023-002-R2-atrimmed.fastq
#call_bbsplit inplanta-3KO-023-003-R1-atrimmed.fastq inplanta-3KO-023-003-R2-atrimmed.fastq
#call_bbsplit inplanta-3KO-023-004-R1-atrimmed.fastq inplanta-3KO-023-004-R2-atrimmed.fastq
#call_bbsplit inplanta-3KO-023-005-R1-atrimmed.fastq inplanta-3KO-023-005-R2-atrimmed.fastq
#call_bbsplit inplanta-3KO-023-006-R1-atrimmed.fastq inplanta-3KO-023-006-R2-atrimmed.fastq
#call_bbsplit inplanta-3KO-023-007-R1-atrimmed.fastq inplanta-3KO-023-007-R2-atrimmed.fastq
call_bbsplit inplanta-3KO-023-008-R1-atrimmed.fastq inplanta-3KO-023-008-R2-atrimmed.fastq
#call_bbsplit inplanta-Pf2-024-001-R1-atrimmed.fastq inplanta-Pf2-024-001-R2-atrimmed.fastq
#call_bbsplit inplanta-Pf2-024-002-R1-atrimmed.fastq inplanta-Pf2-024-002-R2-atrimmed.fastq
#call_bbsplit inplanta-Pf2-024-003-R1-atrimmed.fastq inplanta-Pf2-024-003-R2-atrimmed.fastq
#call_bbsplit inplanta-Pf2-024-004-R1-atrimmed.fastq inplanta-Pf2-024-004-R2-atrimmed.fastq
#call_bbsplit inplanta-Pf2-024-005-R1-atrimmed.fastq inplanta-Pf2-024-005-R2-atrimmed.fastq
#call_bbsplit inplanta-Pf2-024-006-R1-atrimmed.fastq inplanta-Pf2-024-006-R2-atrimmed.fastq
#call_bbsplit inplanta-Pf2-024-007-R1-atrimmed.fastq inplanta-Pf2-024-007-R2-atrimmed.fastq
#call_bbsplit inplanta-Pf2-024-008-R1-atrimmed.fastq inplanta-Pf2-024-008-R2-atrimmed.fastq
#call_bbsplit inplanta-Pf2-025-001-R1-atrimmed.fastq inplanta-Pf2-025-001-R2-atrimmed.fastq
#call_bbsplit inplanta-Pf2-025-002-R1-atrimmed.fastq inplanta-Pf2-025-002-R2-atrimmed.fastq
#call_bbsplit inplanta-Pf2-025-003-R1-atrimmed.fastq inplanta-Pf2-025-003-R2-atrimmed.fastq
#call_bbsplit inplanta-Pf2-025-004-R1-atrimmed.fastq inplanta-Pf2-025-004-R2-atrimmed.fastq
#call_bbsplit inplanta-Pf2-025-005-R1-atrimmed.fastq inplanta-Pf2-025-005-R2-atrimmed.fastq
#call_bbsplit inplanta-Pf2-025-006-R1-atrimmed.fastq inplanta-Pf2-025-006-R2-atrimmed.fastq
#call_bbsplit inplanta-Pf2-025-007-R1-atrimmed.fastq inplanta-Pf2-025-007-R2-atrimmed.fastq
#call_bbsplit inplanta-Pf2-025-008-R1-atrimmed.fastq inplanta-Pf2-025-008-R2-atrimmed.fastq
#call_bbsplit inplanta-Pf2-026-001-R1-atrimmed.fastq inplanta-Pf2-026-001-R2-atrimmed.fastq
#call_bbsplit inplanta-Pf2-026-002-R1-atrimmed.fastq inplanta-Pf2-026-002-R2-atrimmed.fastq
#call_bbsplit inplanta-Pf2-026-003-R1-atrimmed.fastq inplanta-Pf2-026-003-R2-atrimmed.fastq
#call_bbsplit inplanta-Pf2-026-004-R1-atrimmed.fastq inplanta-Pf2-026-004-R2-atrimmed.fastq
#call_bbsplit inplanta-Pf2-026-005-R1-atrimmed.fastq inplanta-Pf2-026-005-R2-atrimmed.fastq
#call_bbsplit inplanta-Pf2-026-006-R1-atrimmed.fastq inplanta-Pf2-026-006-R2-atrimmed.fastq
#call_bbsplit inplanta-Pf2-026-007-R1-atrimmed.fastq inplanta-Pf2-026-007-R2-atrimmed.fastq
#call_bbsplit inplanta-Pf2-026-008-R1-atrimmed.fastq inplanta-Pf2-026-008-R2-atrimmed.fastq
#call_bbsplit inplanta-Pf2-027-001-R1-atrimmed.fastq inplanta-Pf2-027-001-R2-atrimmed.fastq
#call_bbsplit inplanta-Pf2-027-002-R1-atrimmed.fastq inplanta-Pf2-027-002-R2-atrimmed.fastq
#call_bbsplit inplanta-Pf2-027-003-R1-atrimmed.fastq inplanta-Pf2-027-003-R2-atrimmed.fastq
#call_bbsplit inplanta-Pf2-027-004-R1-atrimmed.fastq inplanta-Pf2-027-004-R2-atrimmed.fastq
#call_bbsplit inplanta-Pf2-027-005-R1-atrimmed.fastq inplanta-Pf2-027-005-R2-atrimmed.fastq
#call_bbsplit inplanta-Pf2-027-006-R1-atrimmed.fastq inplanta-Pf2-027-006-R2-atrimmed.fastq
#call_bbsplit inplanta-Pf2-027-007-R1-atrimmed.fastq inplanta-Pf2-027-007-R2-atrimmed.fastq
#call_bbsplit inplanta-Pf2-027-008-R1-atrimmed.fastq inplanta-Pf2-027-008-R2-atrimmed.fastq
#call_bbsplit inplanta-SN15-016-001-R1-atrimmed.fastq inplanta-SN15-016-001-R2-atrimmed.fastq
#call_bbsplit inplanta-SN15-016-002-R1-atrimmed.fastq inplanta-SN15-016-002-R2-atrimmed.fastq
#call_bbsplit inplanta-SN15-016-003-R1-atrimmed.fastq inplanta-SN15-016-003-R2-atrimmed.fastq
#call_bbsplit inplanta-SN15-016-004-R1-atrimmed.fastq inplanta-SN15-016-004-R2-atrimmed.fastq
#call_bbsplit inplanta-SN15-016-005-R1-atrimmed.fastq inplanta-SN15-016-005-R2-atrimmed.fastq
#call_bbsplit inplanta-SN15-016-006-R1-atrimmed.fastq inplanta-SN15-016-006-R2-atrimmed.fastq
#call_bbsplit inplanta-SN15-016-007-R1-atrimmed.fastq inplanta-SN15-016-007-R2-atrimmed.fastq
#call_bbsplit inplanta-SN15-017-001-R1-atrimmed.fastq inplanta-SN15-017-001-R2-atrimmed.fastq
#call_bbsplit inplanta-SN15-017-002-R1-atrimmed.fastq inplanta-SN15-017-002-R2-atrimmed.fastq
#call_bbsplit inplanta-SN15-017-003-R1-atrimmed.fastq inplanta-SN15-017-003-R2-atrimmed.fastq
#call_bbsplit inplanta-SN15-017-004-R1-atrimmed.fastq inplanta-SN15-017-004-R2-atrimmed.fastq
#call_bbsplit inplanta-SN15-017-005-R1-atrimmed.fastq inplanta-SN15-017-005-R2-atrimmed.fastq
#call_bbsplit inplanta-SN15-017-006-R1-atrimmed.fastq inplanta-SN15-017-006-R2-atrimmed.fastq
#call_bbsplit inplanta-SN15-017-007-R1-atrimmed.fastq inplanta-SN15-017-007-R2-atrimmed.fastq
#call_bbsplit inplanta-SN15-018-001-R1-atrimmed.fastq inplanta-SN15-018-001-R2-atrimmed.fastq
#call_bbsplit inplanta-SN15-018-002-R1-atrimmed.fastq inplanta-SN15-018-002-R2-atrimmed.fastq
#call_bbsplit inplanta-SN15-018-003-R1-atrimmed.fastq inplanta-SN15-018-003-R2-atrimmed.fastq
#call_bbsplit inplanta-SN15-018-004-R1-atrimmed.fastq inplanta-SN15-018-004-R2-atrimmed.fastq
#call_bbsplit inplanta-SN15-018-005-R1-atrimmed.fastq inplanta-SN15-018-005-R2-atrimmed.fastq
#call_bbsplit inplanta-SN15-018-006-R1-atrimmed.fastq inplanta-SN15-018-006-R2-atrimmed.fastq
#call_bbsplit inplanta-SN15-018-007-R1-atrimmed.fastq inplanta-SN15-018-007-R2-atrimmed.fastq
#call_bbsplit inplanta-SN15-019-001-R1-atrimmed.fastq inplanta-SN15-019-001-R2-atrimmed.fastq
#call_bbsplit inplanta-SN15-019-002-R1-atrimmed.fastq inplanta-SN15-019-002-R2-atrimmed.fastq
#call_bbsplit inplanta-SN15-019-003-R1-atrimmed.fastq inplanta-SN15-019-003-R2-atrimmed.fastq
#call_bbsplit inplanta-SN15-019-004-R1-atrimmed.fastq inplanta-SN15-019-004-R2-atrimmed.fastq
#call_bbsplit inplanta-SN15-019-005-R1-atrimmed.fastq inplanta-SN15-019-005-R2-atrimmed.fastq
#call_bbsplit inplanta-SN15-019-006-R1-atrimmed.fastq inplanta-SN15-019-006-R2-atrimmed.fastq
#call_bbsplit inplanta-SN15-019-007-R1-atrimmed.fastq inplanta-SN15-019-007-R2-atrimmed.fastq
#call_bbsplit inplanta-SN15-019-008-R1-atrimmed.fastq inplanta-SN15-019-008-R2-atrimmed.fastq
#call_bbsplit inplanta-Tween-012-001-R1-atrimmed.fastq inplanta-Tween-012-001-R2-atrimmed.fastq
#call_bbsplit inplanta-Tween-012-002-R1-atrimmed.fastq inplanta-Tween-012-002-R2-atrimmed.fastq
#call_bbsplit inplanta-Tween-012-003-R1-atrimmed.fastq inplanta-Tween-012-003-R2-atrimmed.fastq
#call_bbsplit inplanta-Tween-012-004-R1-atrimmed.fastq inplanta-Tween-012-004-R2-atrimmed.fastq
#call_bbsplit inplanta-Tween-012-005-R1-atrimmed.fastq inplanta-Tween-012-005-R2-atrimmed.fastq
#call_bbsplit inplanta-Tween-012-006-R1-atrimmed.fastq inplanta-Tween-012-006-R2-atrimmed.fastq
#call_bbsplit inplanta-Tween-012-007-R1-atrimmed.fastq inplanta-Tween-012-007-R2-atrimmed.fastq
#call_bbsplit inplanta-Tween-012-008-R1-atrimmed.fastq inplanta-Tween-012-008-R2-atrimmed.fastq
#call_bbsplit inplanta-Tween-013-001-R1-atrimmed.fastq inplanta-Tween-013-001-R2-atrimmed.fastq
#call_bbsplit inplanta-Tween-013-002-R1-atrimmed.fastq inplanta-Tween-013-002-R2-atrimmed.fastq
#call_bbsplit inplanta-Tween-013-003-R1-atrimmed.fastq inplanta-Tween-013-003-R2-atrimmed.fastq
#call_bbsplit inplanta-Tween-013-004-R1-atrimmed.fastq inplanta-Tween-013-004-R2-atrimmed.fastq
#call_bbsplit inplanta-Tween-013-005-R1-atrimmed.fastq inplanta-Tween-013-005-R2-atrimmed.fastq
#call_bbsplit inplanta-Tween-013-006-R1-atrimmed.fastq inplanta-Tween-013-006-R2-atrimmed.fastq
#call_bbsplit inplanta-Tween-013-007-R1-atrimmed.fastq inplanta-Tween-013-007-R2-atrimmed.fastq
#call_bbsplit inplanta-Tween-013-008-R1-atrimmed.fastq inplanta-Tween-013-008-R2-atrimmed.fastq
#call_bbsplit inplanta-Tween-014-001-R1-atrimmed.fastq inplanta-Tween-014-001-R2-atrimmed.fastq
#call_bbsplit inplanta-Tween-014-002-R1-atrimmed.fastq inplanta-Tween-014-002-R2-atrimmed.fastq
#call_bbsplit inplanta-Tween-014-003-R1-atrimmed.fastq inplanta-Tween-014-003-R2-atrimmed.fastq
#call_bbsplit inplanta-Tween-014-004-R1-atrimmed.fastq inplanta-Tween-014-004-R2-atrimmed.fastq
#call_bbsplit inplanta-Tween-014-005-R1-atrimmed.fastq inplanta-Tween-014-005-R2-atrimmed.fastq
#call_bbsplit inplanta-Tween-014-006-R1-atrimmed.fastq inplanta-Tween-014-006-R2-atrimmed.fastq
#call_bbsplit inplanta-Tween-014-007-R1-atrimmed.fastq inplanta-Tween-014-007-R2-atrimmed.fastq
#call_bbsplit inplanta-Tween-015-001-R1-atrimmed.fastq inplanta-Tween-015-001-R2-atrimmed.fastq
#call_bbsplit inplanta-Tween-015-002-R1-atrimmed.fastq inplanta-Tween-015-002-R2-atrimmed.fastq
#call_bbsplit inplanta-Tween-015-003-R1-atrimmed.fastq inplanta-Tween-015-003-R2-atrimmed.fastq
#call_bbsplit inplanta-Tween-015-004-R1-atrimmed.fastq inplanta-Tween-015-004-R2-atrimmed.fastq
#call_bbsplit inplanta-Tween-015-005-R1-atrimmed.fastq inplanta-Tween-015-005-R2-atrimmed.fastq
#call_bbsplit inplanta-Tween-015-006-R1-atrimmed.fastq inplanta-Tween-015-006-R2-atrimmed.fastq
#call_bbsplit inplanta-Tween-015-007-R1-atrimmed.fastq inplanta-Tween-015-007-R2-atrimmed.fastq
#call_bbsplit inplanta-Tween-015-008-R1-atrimmed.fastq inplanta-Tween-015-008-R2-atrimmed.fastq
