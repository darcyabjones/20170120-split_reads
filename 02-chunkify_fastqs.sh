ADAPTER_DIR=01-trimmed_adapters
CHUNKIFY_DIR=02-chunkify_fastqs

# inplanta-3KO-020-R2.fastq.gz
# inplanta-3KO-021-R2.fastq.gz
# inplanta-3KO-022-R2.fastq.gz
# inplanta-3KO-023-R2.fastq.gz
# inplanta-Pf2-024-R2.fastq.gz
# inplanta-Pf2-025-R2.fastq.gz
# inplanta-Pf2-026-R2.fastq.gz
# inplanta-Pf2-027-R2.fastq.gz
# inplanta-SN15-016-R2.fastq.gz
# inplanta-SN15-017-R2.fastq.gz
# inplanta-SN15-018-R2.fastq.gz
# inplanta-SN15-019-R2.fastq.gz
# inplanta-Tween-012-R2.fastq.gz
# inplanta-Tween-013-R2.fastq.gz
# inplanta-Tween-014-R2.fastq.gz
# inplanta-Tween-015-R2.fastq.gz
# invitro-3KO-004-R2.fastq.gz
# invitro-3KO-005-R2.fastq.gz
# invitro-3KO-006-R2.fastq.gz
# invitro-3KO-007-R2.fastq.gz
# invitro-Pf2-008-R2.fastq.gz
# invitro-Pf2-009-R2.fastq.gz
# invitro-Pf2-010-R2.fastq.gz
# invitro-Pf2-011-R2.fastq.gz
# invitro-SN15-000-R2.fastq.gz
# invitro-SN15-001-R2.fastq.gz
# invitro-SN15-002-R2.fastq.gz
# invitro-SN15-003-R2.fastq.gz

zcat "${ADAPTER_DIR}/inplanta-3KO-020-R1-atrimmed.fastq.gz" | python bin/splitter.py -n 40000000 --pattern "^@" -p "${CHUNKIFY_DIR}/inplanta-3KO-020-{d}-R1-atrimmed.fastq" &
zcat "${ADAPTER_DIR}/inplanta-3KO-021-R1-atrimmed.fastq.gz" | python bin/splitter.py -n 40000000 --pattern "^@" -p "${CHUNKIFY_DIR}/inplanta-3KO-021-{d}-R1-atrimmed.fastq" &
zcat "${ADAPTER_DIR}/inplanta-3KO-022-R1-atrimmed.fastq.gz" | python bin/splitter.py -n 40000000 --pattern "^@" -p "${CHUNKIFY_DIR}/inplanta-3KO-022-{d}-R1-atrimmed.fastq" &
zcat "${ADAPTER_DIR}/inplanta-3KO-023-R1-atrimmed.fastq.gz" | python bin/splitter.py -n 40000000 --pattern "^@" -p "${CHUNKIFY_DIR}/inplanta-3KO-023-{d}-R1-atrimmed.fastq" &
zcat "${ADAPTER_DIR}/inplanta-Pf2-024-R1-atrimmed.fastq.gz" | python bin/splitter.py -n 40000000 --pattern "^@" -p "${CHUNKIFY_DIR}/inplanta-Pf2-024-{d}-R1-atrimmed.fastq" &
zcat "${ADAPTER_DIR}/inplanta-Pf2-025-R1-atrimmed.fastq.gz" | python bin/splitter.py -n 40000000 --pattern "^@" -p "${CHUNKIFY_DIR}/inplanta-Pf2-025-{d}-R1-atrimmed.fastq" &
zcat "${ADAPTER_DIR}/inplanta-Pf2-026-R1-atrimmed.fastq.gz" | python bin/splitter.py -n 40000000 --pattern "^@" -p "${CHUNKIFY_DIR}/inplanta-Pf2-026-{d}-R1-atrimmed.fastq" &
zcat "${ADAPTER_DIR}/inplanta-Pf2-027-R1-atrimmed.fastq.gz" | python bin/splitter.py -n 40000000 --pattern "^@" -p "${CHUNKIFY_DIR}/inplanta-Pf2-027-{d}-R1-atrimmed.fastq" &
zcat "${ADAPTER_DIR}/inplanta-SN15-016-R1-atrimmed.fastq.gz" | python bin/splitter.py -n 40000000 --pattern "^@" -p "${CHUNKIFY_DIR}/inplanta-SN15-016-{d}-R1-atrimmed.fastq" &
zcat "${ADAPTER_DIR}/inplanta-SN15-017-R1-atrimmed.fastq.gz" | python bin/splitter.py -n 40000000 --pattern "^@" -p "${CHUNKIFY_DIR}/inplanta-SN15-017-{d}-R1-atrimmed.fastq" &
zcat "${ADAPTER_DIR}/inplanta-SN15-018-R1-atrimmed.fastq.gz" | python bin/splitter.py -n 40000000 --pattern "^@" -p "${CHUNKIFY_DIR}/inplanta-SN15-018-{d}-R1-atrimmed.fastq" &
zcat "${ADAPTER_DIR}/inplanta-SN15-019-R1-atrimmed.fastq.gz" | python bin/splitter.py -n 40000000 --pattern "^@" -p "${CHUNKIFY_DIR}/inplanta-SN15-019-{d}-R1-atrimmed.fastq" &
zcat "${ADAPTER_DIR}/inplanta-Tween-012-R1-atrimmed.fastq.gz" | python bin/splitter.py -n 40000000 --pattern "^@" -p "${CHUNKIFY_DIR}/inplanta-Tween-012-{d}-R1-atrimmed.fastq" &
zcat "${ADAPTER_DIR}/inplanta-Tween-013-R1-atrimmed.fastq.gz" | python bin/splitter.py -n 40000000 --pattern "^@" -p "${CHUNKIFY_DIR}/inplanta-Tween-013-{d}-R1-atrimmed.fastq" &
zcat "${ADAPTER_DIR}/inplanta-Tween-014-R1-atrimmed.fastq.gz" | python bin/splitter.py -n 40000000 --pattern "^@" -p "${CHUNKIFY_DIR}/inplanta-Tween-014-{d}-R1-atrimmed.fastq" &
zcat "${ADAPTER_DIR}/inplanta-Tween-015-R1-atrimmed.fastq.gz" | python bin/splitter.py -n 40000000 --pattern "^@" -p "${CHUNKIFY_DIR}/inplanta-Tween-015-{d}-R1-atrimmed.fastq" &

wait

zcat "${ADAPTER_DIR}/inplanta-3KO-020-R2-atrimmed.fastq.gz" | python bin/splitter.py -n 40000000 --pattern "^@" -p "${CHUNKIFY_DIR}/inplanta-3KO-020-{d}-R2-atrimmed.fastq" &
zcat "${ADAPTER_DIR}/inplanta-3KO-021-R2-atrimmed.fastq.gz" | python bin/splitter.py -n 40000000 --pattern "^@" -p "${CHUNKIFY_DIR}/inplanta-3KO-021-{d}-R2-atrimmed.fastq" &
zcat "${ADAPTER_DIR}/inplanta-3KO-022-R2-atrimmed.fastq.gz" | python bin/splitter.py -n 40000000 --pattern "^@" -p "${CHUNKIFY_DIR}/inplanta-3KO-022-{d}-R2-atrimmed.fastq" &
zcat "${ADAPTER_DIR}/inplanta-3KO-023-R2-atrimmed.fastq.gz" | python bin/splitter.py -n 40000000 --pattern "^@" -p "${CHUNKIFY_DIR}/inplanta-3KO-023-{d}-R2-atrimmed.fastq" &
zcat "${ADAPTER_DIR}/inplanta-Pf2-024-R2-atrimmed.fastq.gz" | python bin/splitter.py -n 40000000 --pattern "^@" -p "${CHUNKIFY_DIR}/inplanta-Pf2-024-{d}-R2-atrimmed.fastq" &
zcat "${ADAPTER_DIR}/inplanta-Pf2-025-R2-atrimmed.fastq.gz" | python bin/splitter.py -n 40000000 --pattern "^@" -p "${CHUNKIFY_DIR}/inplanta-Pf2-025-{d}-R2-atrimmed.fastq" &
zcat "${ADAPTER_DIR}/inplanta-Pf2-026-R2-atrimmed.fastq.gz" | python bin/splitter.py -n 40000000 --pattern "^@" -p "${CHUNKIFY_DIR}/inplanta-Pf2-026-{d}-R2-atrimmed.fastq" &
zcat "${ADAPTER_DIR}/inplanta-Pf2-027-R2-atrimmed.fastq.gz" | python bin/splitter.py -n 40000000 --pattern "^@" -p "${CHUNKIFY_DIR}/inplanta-Pf2-027-{d}-R2-atrimmed.fastq" &
zcat "${ADAPTER_DIR}/inplanta-SN15-016-R2-atrimmed.fastq.gz" | python bin/splitter.py -n 40000000 --pattern "^@" -p "${CHUNKIFY_DIR}/inplanta-SN15-016-{d}-R2-atrimmed.fastq" &
zcat "${ADAPTER_DIR}/inplanta-SN15-017-R2-atrimmed.fastq.gz" | python bin/splitter.py -n 40000000 --pattern "^@" -p "${CHUNKIFY_DIR}/inplanta-SN15-017-{d}-R2-atrimmed.fastq" &
zcat "${ADAPTER_DIR}/inplanta-SN15-018-R2-atrimmed.fastq.gz" | python bin/splitter.py -n 40000000 --pattern "^@" -p "${CHUNKIFY_DIR}/inplanta-SN15-018-{d}-R2-atrimmed.fastq" &
zcat "${ADAPTER_DIR}/inplanta-SN15-019-R2-atrimmed.fastq.gz" | python bin/splitter.py -n 40000000 --pattern "^@" -p "${CHUNKIFY_DIR}/inplanta-SN15-019-{d}-R2-atrimmed.fastq" &
zcat "${ADAPTER_DIR}/inplanta-Tween-012-R2-atrimmed.fastq.gz" | python bin/splitter.py -n 40000000 --pattern "^@" -p "${CHUNKIFY_DIR}/inplanta-Tween-012-{d}-R2-atrimmed.fastq" &
zcat "${ADAPTER_DIR}/inplanta-Tween-013-R2-atrimmed.fastq.gz" | python bin/splitter.py -n 40000000 --pattern "^@" -p "${CHUNKIFY_DIR}/inplanta-Tween-013-{d}-R2-atrimmed.fastq" &
zcat "${ADAPTER_DIR}/inplanta-Tween-014-R2-atrimmed.fastq.gz" | python bin/splitter.py -n 40000000 --pattern "^@" -p "${CHUNKIFY_DIR}/inplanta-Tween-014-{d}-R2-atrimmed.fastq" &
zcat "${ADAPTER_DIR}/inplanta-Tween-015-R2-atrimmed.fastq.gz" | python bin/splitter.py -n 40000000 --pattern "^@" -p "${CHUNKIFY_DIR}/inplanta-Tween-015-{d}-R2-atrimmed.fastq" &

wait
