SPLIT_DIR="03-split_by_reference"

PN_GENOME_FILE=data/SN15v9_OM_nonewline.fasta
PN_GENOME_NAME=pn
TA_GENOME_FILE=data/Triticum_aestivum.TGACv1.dna_sm.toplevel.fa
TA_GENOME_NAME=ta

# May need to adjust memory allowance
bbsplit.sh \
	build=1 \
	k=13 \
	path=${SPLIT_DIR}/index \
	ref_${TA_GENOME_NAME}=${TA_GENOME_FILE} \
	ref_${PN_GENOME_NAME}=${PN_GENOME_FILE}


